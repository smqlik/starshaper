const fs = require('fs');

let colors = ["Red","Yellow","Blue","Orange","Green","Purple","Black","Yellow"];
let genders = ["F","M"];
let sizes = ["S","M","L","XL"];
let styles = ["Graphic","Plain","Striped","V-Neck"];
let prices = [5.25,10.00,12.99,15.00];

let accounts = ['0015B00001BsFWYQA3','0015B00001BsFVSQA3','0015B00001BsFSeQAN','0015B00001BsFaBQAV','0015B00001BsFb9QAF'];
let contacts = {
    '0015B00001BsFWYQA3': ['0035B00000jlbw1QAA','0035B00000jlbyvQAA','0035B00000jlbwNQAQ'],
    '0015B00001BsFVSQA3': ['0035B00000jlbzHQAQ','0035B00000jlbpeQAA','0035B00000jlcF7QAI'],
    '0015B00001BsFSeQAN': ['0035B00000jlcFgQAI','0035B00000jlcGAQAY','0035B00000jlbwRQAQ'],
    '0015B00001BsFaBQAV': ['0035B00000jlbzIQAQ','0035B00000jlcJbQAI','0035B00000jlcTsQAI'],
    '0015B00001BsFb9QAF': ['0035B00000jlcRiQAI','0035B00000jlcTvQAI','0035B00000jlcZRQAY']
}; 


let startDate = new Date(2019, 0, 1, 0, 0, 0, 0);
let days = 366;

var dataStream = fs.createWriteStream('./tshirtsales.csv');
dataStream.write(
    'RecordId,Date,AccountId,ContactId,Color,Gender,Size,Style,Price\n'
);


function getRandomInt(min, max){
    return Math.floor((Math.floor(max-min) * Math.random())+min);
}

console.log('generating');

recordNumber = 0; 

for(let i=0; i<days; ++i) {


    let sales = getRandomInt(10,500);

    console.log(sales);

    for(let j=0; j<sales; ++j) {
        let accountNumber = getRandomInt(0, accounts.length);
        let accountId = accounts[accountNumber];
        let contactId = contacts[accountId][getRandomInt(0,3)];
        let color = colors[getRandomInt(0, colors.length)];
        let gender = genders[getRandomInt(0, genders.length)];
        let size = sizes[getRandomInt(0, sizes.length)];
        let style = styles[getRandomInt(0, styles.length)];
        let price = prices[getRandomInt(0, prices.length)];

        dataStream.write(
            recordNumber+','+
            startDate.toISOString()+','+
            accountId+','+
            contactId+','+
            color+','+
            gender+','+
            size+','+
            style+','+
            price+'\n'
        );

        ++recordNumber;

    }

    startDate.setDate(startDate.getDate() + 1);
}

dataStream.close();













