import { LightningElement, api } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import enigma from '@salesforce/resourceUrl/enigma';
import nebula from '@salesforce/resourceUrl/nebula';
import linechart from '@salesforce/resourceUrl/snLineChart';
import kpichart from '@salesforce/resourceUrl/snKPI';

export default class CatsEye extends LightningElement {


    tenantUri = 'https://smapitest.us.qlikcloud.com';
    webIntegrationId = '-cUBeDwVNfjV71YLBuRp6JzxjzFfh16I';
    enigmaInitialized = false; 

    username;
    appname; 
    csrf; 

    @api recordId; 
    regeneratorRuntime;

    async request(path, returnJson = true) {
        const res = await fetch(`${this.tenantUri}${path}`, {
            mode: 'cors',
            credentials: 'include',
            redirect: 'follow',
            headers: {
                'qlik-web-integration-id': this.webIntegrationId
            }
        });

        if(res.status < 200 || res.status >= 400) {
            throw res; 
        } else {
            return returnJson ? res.json() : res; 
        }

    }

    async renderedCallback() {

        if(this.enigmaInitialized) {
            return; 
        }

        this.enigmaInitialized = true; 
        
        await loadScript(this, enigma).then(async () => {
            console.log('enigma script loaded');

            await loadScript(this, nebula).then(async () => {
                
                this.regeneratorRuntime = stardust.regeneratorRuntime;
                
                console.log('nebula script loaded');

                await loadScript(this, linechart).then( () => {
                    
                });

                await loadScript(this, kpichart).then( () => {
                    
                });

            });
        
        });
        

        try {
            const user = await this.request('/api/v1/users/me');
            console.log(user);
            this.username = user.name;
            
            try {

                const res = await this.request('/api/v1/csrf-token', false);
                this.csrf = res.headers.get('qlik-csrf-token');

                /* const apps = await this.request('/api/v1/items?resourceType=app');
                console.log(apps.data.length);

                if(!apps.data.length) {
                    this.appname = 'No apps available';
                    return; 
                }*/
                
                const appId = 'b28fca0a-0213-4203-bb21-4c845bac1b35';
                console.log(appId);

                const url = `${this.tenantUri.replace('https','wss')}/app/${appId}?qlik-web-integration-id=${this.webIntegrationId}&qlik-csrf-token=${this.csrf}`;
                
                const schema = await ( 
                    await fetch('https://unpkg.com/enigma.js@2.7.0/schemas/12.612.0.json')
                ).json();


    
                    const session = window.enigma.create({url, schema});
                    const global = await session.open();
                    const app = await global.openDoc(appId);
                    const appLayout = await app.getAppLayout(); 
        
                    this.appname = appLayout.qTitle;

                    console.log('setting up embed');

                    console.log('recordId : '+this.recordId);

                    const n = stardust.embed(app, {
                        types: [
                            {
                                name: 'linechart',
                                load: () => Promise.resolve(line)
                            },
                            {
                                name: 'kpichart',
                                load: () => Promise.resolve(kpi)   
                            }
                        ]
                    });
                
                    
                    n.render({
                        element: this.template.querySelector('.total'),
                        type: 'kpichart',
                        fields: ['=Count({<AccountId={\''+this.recordId+'\'}>}RecordId)'],
                        properties: {
                            showTitles: true,
                            title: "Total Sales",
                            showMeasureTitle: false
                        }
                    })
                    
                    
                    n.render({
                        element: this.template.querySelector('.object'),
                        type: 'linechart',
                        fields: ['Date.autoCalendar.Date','=Count({<AccountId={\''+this.recordId+'\'}>}RecordId)'],
                    });

                    


            } catch (err) {
                console.log(err);
            }

        } catch (err) {
            console.log(err);
        } 

    }

}