import { LightningElement, api } from 'lwc';

export default class LittleGhost extends LightningElement {

    @api qlikEmbedURI;
    
    @api height;
    @api width; 
   
    get style(){
        return "border:none;width:"+(this.width?this.width:'100%')+";height:"+(this.height?this.height:'100%')+";"
    }


   
}