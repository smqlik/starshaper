import { LightningElement, api, track } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import enigma from '@salesforce/resourceUrl/enigma';
import nebula from '@salesforce/resourceUrl/nebula';
import linechart from '@salesforce/resourceUrl/snLineChart';


export default class Boomerang extends LightningElement {


    tenantUri = 'https://smapitest.us.qlikcloud.com';
    webIntegrationId = '-cUBeDwVNfjV71YLBuRp6JzxjzFfh16I';
    enigmaInitialized = false; 

    username;
    appname; 
    csrf; 


    columns = [
        {label: 'Id', fieldName: 'Id', type: 'number'},
        {label: 'Order Date', fieldName: 'Date', type: 'datetime'},
        {label: 'Account Id', fieldName: 'AccountId', type: 'id'},
        {label: 'Contact Id', fieldName: 'ContactId', type: 'id'},
        {label: 'Color', fieldName: 'Color', type: 'text'},
        {label: 'Gender', fieldName: 'Gender', type: 'text'},
        {label: 'Size', fieldName: 'Size', type: 'text'},
        {label: 'Style', fieldName: 'Style', type: 'text'},
        {label: 'Price', fieldName: 'Price', type: 'currency'},
    ];
    @track tablerows;


    @api recordId; 
    regeneratorRuntime;


    async request(path, returnJson = true) {
        const res = await fetch(`${this.tenantUri}${path}`, {
            mode: 'cors',
            credentials: 'include',
            redirect: 'follow',
            headers: {
                'qlik-web-integration-id': this.webIntegrationId
            }
        });

        if(res.status < 200 || res.status >= 400) {
            throw res; 
        } else {
            return returnJson ? res.json() : res; 
        }

    }
    
    async renderedCallback() {

        if(this.enigmaInitialized) {
            return; 
        }

        this.enigmaInitialized = true; 
        
        await loadScript(this, enigma).then(async () => {
            console.log('enigma script loaded');        
        });
        

        try {
            const user = await this.request('/api/v1/users/me');
            console.log(user);
            this.username = user.name;
            
            try {

                const res = await this.request('/api/v1/csrf-token', false);
                this.csrf = res.headers.get('qlik-csrf-token');

                /* const apps = await this.request('/api/v1/items?resourceType=app');
                console.log(apps.data.length);

                if(!apps.data.length) {
                    this.appname = 'No apps available';
                    return; 
                }*/
                
                const appId = 'b28fca0a-0213-4203-bb21-4c845bac1b35';
                console.log(appId);

                const url = `${this.tenantUri.replace('https','wss')}/app/${appId}?qlik-web-integration-id=${this.webIntegrationId}&qlik-csrf-token=${this.csrf}`;
                
                const schema = await ( 
                    await fetch('https://unpkg.com/enigma.js@2.7.0/schemas/12.612.0.json')
                ).json();

                const session = window.enigma.create({url, schema});
                const global = await session.open();
                const app = await global.openDoc(appId);
                const appLayout = await app.getAppLayout(); 
        

                const infos = await app.getAllInfos();

                app.getTableData(0, 10, true, 'tshirtsales').then(qdata => {
                    
                    let rows = [];
                    for(let i=0; i<qdata.length; ++i) {

                        console.log(qdata[i]);
                    
                        rows.push({
                            Id: qdata[i].qValue[0].qText,
                            Date: qdata[i].qValue[1].qText, 
                            AccountId: qdata[i].qValue[2].qText,
                            ContactId: qdata[i].qValue[3].qText,
                            Color: qdata[i].qValue[4].qText, 
                            Gender: qdata[i].qValue[5].qText,
                            Size: qdata[i].qValue[6].qText,
                            Style: qdata[i].qValue[7].qText,
                            Price: qdata[i].qValue[8].qText
                        });
                    }
                    this.tablerows = rows; 
                }).catch(error => {
                    console.log(error);
                })

                console.log(infos);
 

                this.appname = appLayout.qTitle;
                
            



            } catch (err) {
                console.log(err);
            }

        } catch (err) {
            console.log(err);
        } 

    }   
    
    
    getSelectedName(event) {
        const selectedRows = event.detail.selectedRows;
        // Display that fieldName of the selected rows
        for (let i = 0; i < selectedRows.length; i++){
            alert("You selected: " + selectedRows[i].Id);
        }
    }


}