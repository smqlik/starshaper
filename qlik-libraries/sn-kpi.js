/*
 * @nebula.js/sn-kpi v0.4.1
 * Copyright (c) 2020 QlikTech International AB
 * Released under the MIT license.
 */

!(function (global, e, t) {
  "object" == typeof exports && "undefined" != typeof module
    ? (module.exports = t(require("@nebula.js/stardust")))
    : "function" == typeof define && define.amd
    ? define(["@nebula.js/stardust"], t)
    : ((e = e || self)["sn-kpi"] = t(e.stardust));

    global.kpi = e["sn-kpi"]; 

})(this, this, function (e) {
  "use strict";
  function t(e) {
    return (t =
      "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
        ? function (e) {
            return typeof e;
          }
        : function (e) {
            return e &&
              "function" == typeof Symbol &&
              e.constructor === Symbol &&
              e !== Symbol.prototype
              ? "symbol"
              : typeof e;
          })(e);
  }
  function n(e, t) {
    if (!(e instanceof t))
      throw new TypeError("Cannot call a class as a function");
  }
  function r(e, t) {
    for (var n = 0; n < t.length; n++) {
      var r = t[n];
      (r.enumerable = r.enumerable || !1),
        (r.configurable = !0),
        "value" in r && (r.writable = !0),
        Object.defineProperty(e, r.key, r);
    }
  }
  function o(e, t, n) {
    return t && r(e.prototype, t), n && r(e, n), e;
  }
  function i(e, t, n) {
    return (
      t in e
        ? Object.defineProperty(e, t, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
          })
        : (e[t] = n),
      e
    );
  }
  function a(e, t) {
    var n = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var r = Object.getOwnPropertySymbols(e);
      t &&
        (r = r.filter(function (t) {
          return Object.getOwnPropertyDescriptor(e, t).enumerable;
        })),
        n.push.apply(n, r);
    }
    return n;
  }
  function s(e) {
    for (var t = 1; t < arguments.length; t++) {
      var n = null != arguments[t] ? arguments[t] : {};
      t % 2
        ? a(Object(n), !0).forEach(function (t) {
            i(e, t, n[t]);
          })
        : Object.getOwnPropertyDescriptors
        ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n))
        : a(Object(n)).forEach(function (t) {
            Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
          });
    }
    return e;
  }
  function l(e, t) {
    return (
      (function (e) {
        if (Array.isArray(e)) return e;
      })(e) ||
      (function (e, t) {
        if (
          !(
            Symbol.iterator in Object(e) ||
            "[object Arguments]" === Object.prototype.toString.call(e)
          )
        )
          return;
        var n = [],
          r = !0,
          o = !1,
          i = void 0;
        try {
          for (
            var a, s = e[Symbol.iterator]();
            !(r = (a = s.next()).done) &&
            (n.push(a.value), !t || n.length !== t);
            r = !0
          );
        } catch (e) {
          (o = !0), (i = e);
        } finally {
          try {
            r || null == s.return || s.return();
          } finally {
            if (o) throw i;
          }
        }
        return n;
      })(e, t) ||
      (function () {
        throw new TypeError(
          "Invalid attempt to destructure non-iterable instance"
        );
      })()
    );
  }
  var u = {
    version: "0.1.0",
    qHyperCubeDef: {
      qDimensions: [],
      qMeasures: [],
      qSuppressZero: !1,
      qSuppressMissing: !0,
      qInitialDataFetch: [{ qWidth: 500, qHeight: 10 }]
    },
    showTitles: !1,
    title: "",
    subtitle: "",
    footnote: "",
    showDetails: !1,
    showMeasureTitle: !0,
    showSecondMeasureTitle: !0,
    textAlign: "center",
    layoutBehavior: "responsive",
    fontSize: "M",
    useLink: !1,
    sheetLink: "",
    openUrlInNewTab: !0
  };
  function c(e) {
    var n = e.sense && e.sense.theme ? e.sense.theme : {},
      r = function () {
        var e = n.getDataColorSpecials().primary,
          t = n.getDataColorPickerPalettes()[0],
          r = "pyramid" === t.type ? t.colors[t.colors.length - 1] : t.colors,
          o = r.indexOf(e);
        return { index: -1 === o ? Math.min(6, r.length - 1) : o };
      },
      o = function (e) {
        var r;
        return (
          (r =
            "object" === t(e)
              ? e.index
                ? e
                : { index: e.color }
              : { index: e }),
          n.getColorPickerColor(r)
        );
      },
      i = function (e, t, n) {
        var r,
          o = t.split("."),
          i = e;
        if (void 0 === i) return n;
        for (r = 0; r < o.length; ++r) {
          if (void 0 === i[o[r]]) return n;
          i = i[o[r]];
        }
        return i;
      },
      a = function (e, t, n) {
        if (t) {
          for (
            var r = t.split("."), o = e, i = r[r.length - 1], a = 0;
            a < r.length - 1;
            ++a
          )
            void 0 === o[r[a]] && (o[r[a]] = Number.isNaN(+r[a + 1]) ? {} : []),
              (o = o[r[a]]);
          void 0 !== n ? (o[i] = n) : delete o[i];
        }
      };
    function s(e) {
      return i(e, "qDef.conditionalColoring.useBaseColors", !0);
    }
    return {
      definition: {
        type: "items",
        component: "accordion",
        items: {
          data: {
            uses: "data",
            addTranslation: "Properties.AddData",
            items: {
              dimensions: { disabledRef: "", show: !1 },
              measures: {
                disabledRef: "",
                items: {
                  measureAxisMin: {
                    type: "items",
                    ref: "qDef.measureAxis.min",
                    defaultValue: 0
                  },
                  measureAxisMax: {
                    type: "items",
                    ref: "qDef.measureAxis.max",
                    defaultValue: 100
                  },
                  conditionalColoringUseConditionalColoring: {
                    type: "items",
                    ref: "qDef.conditionalColoring.useConditionalColoring",
                    defaultValue: !1
                  },
                  conditionalColoringUseSingleColor: {
                    type: "items",
                    ref: "qDef.conditionalColoring.singleColor",
                    defaultValue: 3
                  },
                  conditionalColoringPaletteSingleColor: {
                    type: "items",
                    ref: "qDef.conditionalColoring.paletteSingleColor",
                    defaultValue: r
                  },
                  limits: {
                    type: "items",
                    ref: "qDef.conditionalColoring.segments.limits",
                    defaultValue: []
                  },
                  paletteColors: {
                    type: "items",
                    ref: "qDef.conditionalColoring.segments.paletteColors",
                    defaultValue: [{ index: 6 }]
                  }
                }
              }
            }
          },
          addons: {
            type: "items",
            component: "expandable-items",
            translation: "properties.addons",
            items: {
              dataHandling: {
                uses: "dataHandling",
                items: { suppressZero: null, calcCond: { uses: "calcCond" } }
              }
            }
          },
          settings: {
            type: "items",
            uses: "settings",
            component: "expandable-items",
            translation: "Common.Appearance",
            items: {
              presentation: {
                type: "items",
                grouped: !1,
                translation: "properties.presentation",
                items: {
                  showMeasureTitle: {
                    ref: "showMeasureTitle",
                    component: "checkbox",
                    translation: "properties.kpi.showFirstTitle",
                    defaultValue: !0
                  },
                  showSecondMeasureTitle: {
                    ref: "showSecondMeasureTitle",
                    component: "checkbox",
                    translation: "properties.kpi.showSecondTitle",
                    defaultValue: !0,
                    show: function (e) {
                      var t = i(e, "qHyperCubeDef.qMeasures");
                      return t && 2 === t.length;
                    }
                  },
                  textAlign: {
                    ref: "textAlign",
                    type: "string",
                    component: "item-selection-list",
                    translation: "properties.Alignment",
                    horizontal: !0,
                    items: [
                      {
                        component: "icon-item",
                        icon: "align_left",
                        labelPlacement: "bottom",
                        value: "left",
                        translation: "properties.dock.left"
                      },
                      {
                        component: "icon-item",
                        icon: "align_center",
                        labelPlacement: "bottom",
                        value: "center",
                        translation: "Common.Center"
                      },
                      {
                        component: "icon-item",
                        icon: "align_right",
                        labelPlacement: "bottom",
                        value: "right",
                        translation: "properties.dock.right"
                      }
                    ],
                    defaultValue: "center"
                  },
                  layoutBehavior: {
                    type: "string",
                    component: "dropdown",
                    ref: "layoutBehavior",
                    translation: "properties.kpi.layoutBehavior",
                    defaultValue: "responsive",
                    undefinedValue: "responsive",
                    snapshot: { tid: "property-layoutBehavior" },
                    options: [
                      {
                        value: "responsive",
                        translation: "properties.responsive"
                      },
                      { value: "relative", translation: "properties.fluid" },
                      { value: "fixed", translation: "properties.fixed" }
                    ],
                    show: function () {
                      return !0;
                    }
                  },
                  fontSize: {
                    ref: "fontSize",
                    type: "string",
                    component: "dropdown",
                    translation: "MediaTool.Toolbar.FontSize",
                    options: [
                      { value: "S", translation: "Common.Small" },
                      { value: "M", translation: "Common.Medium" },
                      { value: "L", translation: "Common.Large" }
                    ],
                    defaultValue: "M"
                  },
                  linkToSheet: {
                    type: "items",
                    items: {
                      useLink: {
                        ref: "useLink",
                        type: "boolean",
                        component: "switch",
                        translation: "properties.kpi.linkToSheet",
                        defaultValue: !1,
                        options: [
                          { value: !0, translation: "properties.on" },
                          { value: !1, translation: "properties.off" }
                        ]
                      },
                      kpiLink: {
                        ref: "sheetLink",
                        type: "string",
                        component: "sheet-dropdown",
                        show: function (e) {
                          return e.useLink;
                        }
                      },
                      openUrlInNewTab: {
                        ref: "openUrlInNewTab",
                        type: "boolean",
                        component: "switch",
                        translation: "properties.kpi.openUrlInNewTab",
                        defaultValue: !0,
                        options: [
                          { value: !0, translation: "properties.on" },
                          { value: !1, translation: "properties.off" }
                        ],
                        show: function (e) {
                          return e.useLink;
                        }
                      }
                    }
                  }
                }
              },
              color: {
                type: "items",
                grouped: !1,
                translation: "properties.color",
                items: {
                  conditionalColoring: {
                    type: "array",
                    component: "measures-tab",
                    ref: "qHyperCubeDef.qMeasures",
                    items: {
                      useConditionalColoring: {
                        ref: "qDef.conditionalColoring.useConditionalColoring",
                        component: "switch",
                        type: "boolean",
                        options: [
                          { value: !0, translation: "properties.on" },
                          { value: !1, translation: "properties.off" }
                        ],
                        translation: "properties.kpi.conditionalColoring",
                        defaultValue: !1,
                        change: function (e) {
                          var t = i(e, "qDef.conditionalColoring"),
                            n =
                              i(
                                e,
                                "qDef.conditionalColoring.segments.paletteColors"
                              ) ||
                              i(e, "qDef.conditionalColoring.segments.colors");
                          t && t.useConditionalColoring && 1 === n.length
                            ? (e.qDef.conditionalColoring.segments.paletteColors = [
                                {
                                  index: -1,
                                  color: o(
                                    e.qDef.conditionalColoring
                                      .paletteSingleColor ||
                                      e.qDef.conditionalColoring.singleColor
                                  )
                                }
                              ])
                            : (e.qDef.conditionalColoring.paletteSingleColor = {
                                index: -1,
                                color: o(n[0])
                              });
                        }
                      },
                      useBaseColor: {
                        ref: "qDef.conditionalColoring.useBaseColors",
                        type: "boolean",
                        defaultValue: !0,
                        translation: "properties.libraryColors",
                        component: "switch",
                        show: function (e, t) {
                          var n = i(t.properties, "color.useBaseColors"),
                            r = i(e, "qDef.conditionalColoring.useBaseColors");
                          return (
                            "off" === n
                              ? (a(t.properties, "color.useBaseColors"),
                                a(
                                  e,
                                  "qDef.conditionalColoring.useBaseColors",
                                  !1
                                ))
                              : void 0 === r &&
                                a(
                                  e,
                                  "qDef.conditionalColoring.useBaseColors",
                                  !0
                                ),
                            !0
                          );
                        },
                        options: [
                          { value: !1, translation: "properties.off" },
                          { value: !0, translation: "properties.on" }
                        ]
                      },
                      percentageWarning: {
                        component: "text",
                        translation: "properties.kpi.librarySegmentWarning",
                        style: "hint",
                        show: function (e) {
                          var t = i(
                              e,
                              "qDef.conditionalColoring.useBaseColors"
                            ),
                            n = i(
                              e,
                              "qDef.conditionalColoring.useConditionalColoring"
                            );
                          return t && n;
                        }
                      },
                      paletteSingleColor: {
                        component: "color-picker",
                        translation: "properties.color",
                        ref: "qDef.conditionalColoring.paletteSingleColor",
                        type: "object",
                        dualOutput: !0,
                        defaultValue: r,
                        show: function (e) {
                          return (
                            !s(e) &&
                            e.qDef.conditionalColoring &&
                            !e.qDef.conditionalColoring.useConditionalColoring
                          );
                        }
                      },
                      rangeLimits: {
                        type: "items",
                        items: {
                          min: {
                            ref: "qDef.measureAxis.min",
                            translation: "properties.axis.min",
                            type: "number"
                          },
                          max: {
                            ref: "qDef.measureAxis.max",
                            translation: "Common.Max",
                            type: "number",
                            defaultValue: 100
                          }
                        },
                        show: function () {
                          return !1;
                        }
                      },
                      segmentInfo: {
                        ref: "qDef.conditionalColoring.segments",
                        type: "items",
                        component: "color-scale-creator",
                        conditionalColoringRef: {
                          props: "qHyperCubeDef.qMeasures",
                          layout: "qHyperCube.qMeasureInfo"
                        },
                        useGlyph: !0,
                        useAutoMinMax: !0,
                        show: function (e) {
                          return (
                            !s(e) &&
                            e.qDef.conditionalColoring.useConditionalColoring
                          );
                        },
                        items: {
                          limits: {
                            ref: "qDef.conditionalColoring.segments.limits",
                            type: "array",
                            items: {
                              value: {
                                ref: "value",
                                type: "number",
                                expression: "optional",
                                invalid: function (e) {
                                  return Number.isNaN(+e.value);
                                },
                                defaultValue: 1
                              },
                              gradient: {
                                translation: "properties.gradient",
                                ref: "gradient",
                                type: "boolean",
                                show: !1
                              }
                            }
                          },
                          paletteColors: {
                            type: "array",
                            ref:
                              "qDef.conditionalColoring.segments.paletteColors",
                            defaultValue: [{ index: 6 }],
                            items: {
                              color: {
                                ref: "color",
                                type: "string",
                                defaultValue: ""
                              },
                              index: { ref: "index", type: "integer" },
                              icon: {
                                ref: "icon",
                                type: "string",
                                defaultValue: ""
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      },
      support: {
        cssScaling: !1,
        snapshot: !0,
        export: !0,
        exportData: !0,
        sharing: !1
      }
    };
  }
  function p(e) {
    return {
      targets: [
        {
          path: "qHyperCubeDef",
          dimensions: { min: 0, max: 0 },
          measures: {
            min: 1,
            max: 2,
            description: function (t, n) {
              var r =
                0 === n
                  ? "Visualizations.Descriptions.KPI.FirstMeasure"
                  : "Visualizations.Descriptions.KPI.SecondMeasure";
              return e.translator.get(r);
            }
          }
        }
      ]
    };
  }
  function f(e, t) {
    var n = (
      f.canvas || (f.canvas = document.createElement("canvas"))
    ).getContext("2d");
    return (n.font = t), n.measureText(e);
  }
  var h = function (e) {
      return 1.5 * f("m", e).width;
    },
    m = function (e, t) {
      var n = f(e, t);
      return void 0 !== e ? Math.ceil(n.width) : 0;
    },
    d = { NONE: 0, ADJUSTTOTEXT: 1 },
    g = { NONE: 0, SCALE: 1, MAX: 2 },
    b = { ROW: "row", COLUMN: "column" };
  function _(e, t, n) {
    var r = e.ratio ? e.ratio : 1,
      o = t / n;
    return e.direction === b.ROW
      ? r > o
        ? b.COLUMN
        : b.ROW
      : r > o
      ? b.ROW
      : b.COLUMN;
  }
  function y(e, t, n, r) {
    var o = arguments.length > 4 && void 0 !== arguments[4] ? arguments[4] : 1,
      i = arguments.length > 5 ? arguments[5] : void 0,
      a = "10px ".concat(i.getStyle("object.kpi", "label.value", "fontFamily")),
      s = h(a) + 1,
      l = r ? r * m("M", a) : m(e, a) * o,
      u = 1.2 * s,
      c = Math.min(t / l, n / u);
    return { fontSize: Math.max(9 * c, 10) };
  }
  var v = {
    checkLayout: function e(t) {
      if (!t.components || 0 === t.components.length) return t;
      var n,
        r,
        o,
        i,
        a,
        s = 0,
        l = t.boundingBox.height,
        u = t.boundingBox.width,
        c = t.direction,
        p = _(t, l, u);
      for (
        t.components.forEach(function (e) {
          (e.direction = e.direction ? e.direction : p),
            (n = e.size.min ? e.size.min : e.size ? e.size : 0),
            ("function" == typeof e.isVisible && !e.isVisible()) ||
            n * l <
              (e.size.treshold && e.size.treshold.height
                ? e.size.treshold.height
                : 10) ||
            u <
              (e.size.treshold && e.size.treshold.width
                ? e.size.treshold.width
                : 10)
              ? (e.show = !1)
              : ((s += n), (e.show = !0));
        }),
          o = t.components.length - 1;
        o >= 0;
        o--
      )
        (i = t.components[o]),
          s < 1
            ? i.size.stretch && (i.size.value = i.size.min + (1 - s))
            : "number" == typeof i.size || (i.size.value = null);
      return (
        t.components.forEach(function (e) {
          (r = e.size.value ? e.size.value : e.size.min ? e.size.min : e.size),
            (a = {}),
            c === b.ROW
              ? ((a.height = l), (a.width = u * r))
              : ((a.width = u), (a.height = l * r)),
            (e.boundingBox = a);
        }),
        t.components.forEach(function (t) {
          e(t);
        }),
        t
      );
    },
    getDirection: _,
    sizeComponent: function e(t, n, r, o, i, a, s) {
      var l,
        u,
        c = t.boundingBox.width,
        p = t.boundingBox.height;
      t.components &&
        t.components.forEach(function (n) {
          e(n, t, c, p, i, a, s);
        }),
        "function" == typeof t.getText &&
          (i === g.MAX
            ? (l = y(t.getText(), c, p, null, null, s))
            : i === g.SCALE
            ? (l = (function (e, t, n, r) {
                if (!e.fontSizes) return { fontSize: "10px" };
                var o = e.getText(),
                  i = e.fontSizes,
                  a = i.min,
                  s = i.max,
                  l = y(o, t, n, i.maxNrChars, e.fontSizes.scale || 1, r),
                  u = Math.min((n - a.height) / (s.height - a.height), 1),
                  c = a.fontSize + u * (s.fontSize - a.fontSize);
                return {
                  fontSize: Math.max(Math.min(l.fontSize, c), a.fontSize)
                };
              })(t, c, p, s))
            : ((u = t.fontSizes.maxNrChars),
              (l = { fontSize: Math.max(310 / u, 10) })),
          a === d.ADJUSTTOTEXT &&
            (t.boundingBox = { height: l.height, width: l.width }),
          t.fontSizes || (t.fontSizes = {}),
          (t.fontSizes.fontSize = "".concat(10 * l.fontSize, "%")),
          (t.fontSizes.scaledFontSize = l.fontSize));
    },
    TextScaleMode: g,
    ElementScaleMode: d,
    Direction: b,
    FixedMobileHeight: 150
  };
  var C,
    k,
    x,
    S,
    w,
    M,
    L,
    N = {},
    q = [],
    D = /acit|ex(?:s|g|n|p|$)|rph|grid|ows|mnc|ntw|ine[ch]|zoo|^ord/i;
  function I(e, t) {
    for (var n in t) e[n] = t[n];
    return e;
  }
  function T(e) {
    var t = e.parentNode;
    t && t.removeChild(e);
  }
  function z(e, t, n) {
    var r,
      o = arguments,
      i = {};
    for (r in t) "key" !== r && "ref" !== r && (i[r] = t[r]);
    if (arguments.length > 3)
      for (n = [n], r = 3; r < arguments.length; r++) n.push(o[r]);
    if (
      (null != n && (i.children = n),
      "function" == typeof e && null != e.defaultProps)
    )
      for (r in e.defaultProps) void 0 === i[r] && (i[r] = e.defaultProps[r]);
    return F(e, i, t && t.key, t && t.ref);
  }
  function F(e, t, n, r) {
    var o = {
      type: e,
      props: t,
      key: n,
      ref: r,
      __k: null,
      __: null,
      __b: 0,
      __e: null,
      __d: void 0,
      __c: null,
      constructor: void 0
    };
    return C.vnode && C.vnode(o), o;
  }
  function E(e) {
    return e.children;
  }
  function O(e, t) {
    (this.props = e), (this.context = t);
  }
  function V(e, t) {
    if (null == t) return e.__ ? V(e.__, e.__.__k.indexOf(e) + 1) : null;
    for (var n; t < e.__k.length; t++)
      if (null != (n = e.__k[t]) && null != n.__e) return n.__e;
    return "function" == typeof e.type ? V(e) : null;
  }
  function A(e) {
    var t, n;
    if (null != (e = e.__) && null != e.__c) {
      for (e.__e = e.__c.base = null, t = 0; t < e.__k.length; t++)
        if (null != (n = e.__k[t]) && null != n.__e) {
          e.__e = e.__c.base = n.__e;
          break;
        }
      return A(e);
    }
  }
  function j(e) {
    ((!e.__d && (e.__d = !0) && k.push(e) && !x++) ||
      w !== C.debounceRendering) &&
      ((w = C.debounceRendering) || S)(P);
  }
  function P() {
    for (var e; (x = k.length); )
      (e = k.sort(function (e, t) {
        return e.__v.__b - t.__v.__b;
      })),
        (k = []),
        e.some(function (e) {
          var t, n, r, o, i, a;
          e.__d &&
            ((i = (o = (t = e).__v).__e),
            (a = t.__P) &&
              ((n = []),
              (r = Z(
                a,
                o,
                I({}, o),
                t.__n,
                void 0 !== a.ownerSVGElement,
                null,
                n,
                null == i ? V(o) : i
              )),
              $(n, o),
              r != i && A(o)));
        });
  }
  function R(e, t, n, r, o, i, a, s, l) {
    var u,
      c,
      p,
      f,
      h,
      m,
      d,
      g = (n && n.__k) || q,
      b = g.length;
    if (
      (s == N && (s = null != i ? i[0] : b ? V(n, 0) : null),
      (u = 0),
      (t.__k = H(t.__k, function (n) {
        if (null != n) {
          if (
            ((n.__ = t),
            (n.__b = t.__b + 1),
            null === (p = g[u]) || (p && n.key == p.key && n.type === p.type))
          )
            g[u] = void 0;
          else
            for (c = 0; c < b; c++) {
              if ((p = g[c]) && n.key == p.key && n.type === p.type) {
                g[c] = void 0;
                break;
              }
              p = null;
            }
          if (
            ((f = Z(e, n, (p = p || N), r, o, i, a, s, l)),
            (c = n.ref) &&
              p.ref != c &&
              (d || (d = []),
              p.ref && d.push(p.ref, null, n),
              d.push(c, n.__c || f, n)),
            null != f)
          ) {
            var _;
            if ((null == m && (m = f), void 0 !== n.__d))
              (_ = n.__d), (n.__d = void 0);
            else if (i == p || f != s || null == f.parentNode) {
              e: if (null == s || s.parentNode !== e)
                e.appendChild(f), (_ = null);
              else {
                for (h = s, c = 0; (h = h.nextSibling) && c < b; c += 2)
                  if (h == f) break e;
                e.insertBefore(f, s), (_ = s);
              }
              "option" == t.type && (e.value = "");
            }
            (s = void 0 !== _ ? _ : f.nextSibling),
              "function" == typeof t.type && (t.__d = s);
          } else s && p.__e == s && s.parentNode != e && (s = V(p));
        }
        return u++, n;
      })),
      (t.__e = m),
      null != i && "function" != typeof t.type)
    )
      for (u = i.length; u--; ) null != i[u] && T(i[u]);
    for (u = b; u--; ) null != g[u] && X(g[u], g[u]);
    if (d) for (u = 0; u < d.length; u++) G(d[u], d[++u], d[++u]);
  }
  function H(e, t, n) {
    if ((null == n && (n = []), null == e || "boolean" == typeof e))
      t && n.push(t(null));
    else if (Array.isArray(e)) for (var r = 0; r < e.length; r++) H(e[r], t, n);
    else
      n.push(
        t
          ? t(
              "string" == typeof e || "number" == typeof e
                ? F(null, e, null, null)
                : null != e.__e || null != e.__c
                ? F(e.type, e.props, e.key, null)
                : e
            )
          : e
      );
    return n;
  }
  function U(e, t, n) {
    "-" === t[0]
      ? e.setProperty(t, n)
      : (e[t] =
          "number" == typeof n && !1 === D.test(t)
            ? n + "px"
            : null == n
            ? ""
            : n);
  }
  function B(e, t, n, r, o) {
    var i, a, s, l, u;
    if (
      (o
        ? "className" === t && (t = "class")
        : "class" === t && (t = "className"),
      "key" === t || "children" === t)
    );
    else if ("style" === t)
      if (((i = e.style), "string" == typeof n)) i.cssText = n;
      else {
        if (("string" == typeof r && ((i.cssText = ""), (r = null)), r))
          for (a in r) (n && a in n) || U(i, a, "");
        if (n) for (s in n) (r && n[s] === r[s]) || U(i, s, n[s]);
      }
    else
      "o" === t[0] && "n" === t[1]
        ? ((l = t !== (t = t.replace(/Capture$/, ""))),
          (u = t.toLowerCase()),
          (t = (u in e ? u : t).slice(2)),
          n
            ? (r || e.addEventListener(t, W, l), ((e.l || (e.l = {}))[t] = n))
            : e.removeEventListener(t, W, l))
        : "list" !== t &&
          "tagName" !== t &&
          "form" !== t &&
          "type" !== t &&
          "size" !== t &&
          !o &&
          t in e
        ? (e[t] = null == n ? "" : n)
        : "function" != typeof n &&
          "dangerouslySetInnerHTML" !== t &&
          (t !== (t = t.replace(/^xlink:?/, ""))
            ? null == n || !1 === n
              ? e.removeAttributeNS(
                  "http://www.w3.org/1999/xlink",
                  t.toLowerCase()
                )
              : e.setAttributeNS(
                  "http://www.w3.org/1999/xlink",
                  t.toLowerCase(),
                  n
                )
            : null == n || (!1 === n && !/^ar/.test(t))
            ? e.removeAttribute(t)
            : e.setAttribute(t, n));
  }
  function W(e) {
    this.l[e.type](C.event ? C.event(e) : e);
  }
  function Z(e, t, n, r, o, i, a, s, l) {
    var u,
      c,
      p,
      f,
      h,
      m,
      d,
      g,
      b,
      _,
      y = t.type;
    if (void 0 !== t.constructor) return null;
    (u = C.__b) && u(t);
    try {
      e: if ("function" == typeof y) {
        if (
          ((g = t.props),
          (b = (u = y.contextType) && r[u.__c]),
          (_ = u ? (b ? b.props.value : u.__) : r),
          n.__c
            ? (d = (c = t.__c = n.__c).__ = c.__E)
            : ("prototype" in y && y.prototype.render
                ? (t.__c = c = new y(g, _))
                : ((t.__c = c = new O(g, _)),
                  (c.constructor = y),
                  (c.render = J)),
              b && b.sub(c),
              (c.props = g),
              c.state || (c.state = {}),
              (c.context = _),
              (c.__n = r),
              (p = c.__d = !0),
              (c.__h = [])),
          null == c.__s && (c.__s = c.state),
          null != y.getDerivedStateFromProps &&
            (c.__s == c.state && (c.__s = I({}, c.__s)),
            I(c.__s, y.getDerivedStateFromProps(g, c.__s))),
          (f = c.props),
          (h = c.state),
          p)
        )
          null == y.getDerivedStateFromProps &&
            null != c.componentWillMount &&
            c.componentWillMount(),
            null != c.componentDidMount && c.__h.push(c.componentDidMount);
        else {
          if (
            (null == y.getDerivedStateFromProps &&
              g !== f &&
              null != c.componentWillReceiveProps &&
              c.componentWillReceiveProps(g, _),
            !c.__e &&
              null != c.shouldComponentUpdate &&
              !1 === c.shouldComponentUpdate(g, c.__s, _))
          ) {
            for (
              c.props = g,
                c.state = c.__s,
                c.__d = !1,
                c.__v = t,
                t.__e = n.__e,
                t.__k = n.__k,
                c.__h.length && a.push(c),
                u = 0;
              u < t.__k.length;
              u++
            )
              t.__k[u] && (t.__k[u].__ = t);
            break e;
          }
          null != c.componentWillUpdate && c.componentWillUpdate(g, c.__s, _),
            null != c.componentDidUpdate &&
              c.__h.push(function () {
                c.componentDidUpdate(f, h, m);
              });
        }
        (c.context = _),
          (c.props = g),
          (c.state = c.__s),
          (u = C.__r) && u(t),
          (c.__d = !1),
          (c.__v = t),
          (c.__P = e),
          (u = c.render(c.props, c.state, c.context)),
          (t.__k =
            null != u && u.type == E && null == u.key
              ? u.props.children
              : Array.isArray(u)
              ? u
              : [u]),
          null != c.getChildContext && (r = I(I({}, r), c.getChildContext())),
          p ||
            null == c.getSnapshotBeforeUpdate ||
            (m = c.getSnapshotBeforeUpdate(f, h)),
          R(e, t, n, r, o, i, a, s, l),
          (c.base = t.__e),
          c.__h.length && a.push(c),
          d && (c.__E = c.__ = null),
          (c.__e = !1);
      } else t.__e = Y(n.__e, t, n, r, o, i, a, l);
      (u = C.diffed) && u(t);
    } catch (e) {
      C.__e(e, t, n);
    }
    return t.__e;
  }
  function $(e, t) {
    C.__c && C.__c(t, e),
      e.some(function (t) {
        try {
          (e = t.__h),
            (t.__h = []),
            e.some(function (e) {
              e.call(t);
            });
        } catch (e) {
          C.__e(e, t.__v);
        }
      });
  }
  function Y(e, t, n, r, o, i, a, s) {
    var l,
      u,
      c,
      p,
      f,
      h = n.props,
      m = t.props;
    if (((o = "svg" === t.type || o), null != i))
      for (l = 0; l < i.length; l++)
        if (
          null != (u = i[l]) &&
          ((null === t.type ? 3 === u.nodeType : u.localName === t.type) ||
            e == u)
        ) {
          (e = u), (i[l] = null);
          break;
        }
    if (null == e) {
      if (null === t.type) return document.createTextNode(m);
      (e = o
        ? document.createElementNS("http://www.w3.org/2000/svg", t.type)
        : document.createElement(t.type, m.is && { is: m.is })),
        (i = null);
    }
    if (null === t.type) h !== m && e.data != m && (e.data = m);
    else if (t !== n) {
      if (
        (null != i && (i = q.slice.call(e.childNodes)),
        (c = (h = n.props || N).dangerouslySetInnerHTML),
        (p = m.dangerouslySetInnerHTML),
        !s)
      ) {
        if (h === N)
          for (h = {}, f = 0; f < e.attributes.length; f++)
            h[e.attributes[f].name] = e.attributes[f].value;
        (p || c) &&
          ((p && c && p.__html == c.__html) ||
            (e.innerHTML = (p && p.__html) || ""));
      }
      (function (e, t, n, r, o) {
        var i;
        for (i in n) i in t || B(e, i, null, n[i], r);
        for (i in t)
          (o && "function" != typeof t[i]) ||
            "value" === i ||
            "checked" === i ||
            n[i] === t[i] ||
            B(e, i, t[i], n[i], r);
      })(e, m, h, o, s),
        (t.__k = t.props.children),
        p || R(e, t, n, r, "foreignObject" !== t.type && o, i, a, N, s),
        s ||
          ("value" in m &&
            void 0 !== m.value &&
            m.value !== e.value &&
            (e.value = null == m.value ? "" : m.value),
          "checked" in m &&
            void 0 !== m.checked &&
            m.checked !== e.checked &&
            (e.checked = m.checked));
    }
    return e;
  }
  function G(e, t, n) {
    try {
      "function" == typeof e ? e(t) : (e.current = t);
    } catch (e) {
      C.__e(e, n);
    }
  }
  function X(e, t, n) {
    var r, o, i;
    if (
      (C.unmount && C.unmount(e),
      (r = e.ref) && ((r.current && r.current !== e.__e) || G(r, null, t)),
      n || "function" == typeof e.type || (n = null != (o = e.__e)),
      (e.__e = e.__d = void 0),
      null != (r = e.__c))
    ) {
      if (r.componentWillUnmount)
        try {
          r.componentWillUnmount();
        } catch (e) {
          C.__e(e, t);
        }
      r.base = r.__P = null;
    }
    if ((r = e.__k)) for (i = 0; i < r.length; i++) r[i] && X(r[i], t, n);
    null != o && T(o);
  }
  function J(e, t, n) {
    return this.constructor(e, n);
  }
  function K(e, t, n) {
    var r, o, i;
    C.__ && C.__(e, t),
      (o = (r = n === M) ? null : (n && n.__k) || t.__k),
      (e = z(E, null, [e])),
      (i = []),
      Z(
        t,
        ((r ? t : n || t).__k = e),
        o || N,
        N,
        void 0 !== t.ownerSVGElement,
        n && !r ? [n] : o ? null : q.slice.call(t.childNodes),
        i,
        n || N,
        r
      ),
      $(i, e);
  }
  function Q(e, t) {
    K(e, t, M);
  }
  function ee(e, t) {
    return (
      (t = I(I({}, e.props), t)),
      arguments.length > 2 && (t.children = q.slice.call(arguments, 2)),
      F(e.type, t, t.key || e.key, t.ref || e.ref)
    );
  }
  (C = {
    __e: function (e, t) {
      for (var n, r; (t = t.__); )
        if ((n = t.__c) && !n.__)
          try {
            if (
              (n.constructor &&
                null != n.constructor.getDerivedStateFromError &&
                ((r = !0),
                n.setState(n.constructor.getDerivedStateFromError(e))),
              null != n.componentDidCatch && ((r = !0), n.componentDidCatch(e)),
              r)
            )
              return j((n.__E = n));
          } catch (t) {
            e = t;
          }
      throw e;
    }
  }),
    (O.prototype.setState = function (e, t) {
      var n;
      (n = this.__s !== this.state ? this.__s : (this.__s = I({}, this.state))),
        "function" == typeof e && (e = e(n, this.props)),
        e && I(n, e),
        null != e && this.__v && (t && this.__h.push(t), j(this));
    }),
    (O.prototype.forceUpdate = function (e) {
      this.__v && ((this.__e = !0), e && this.__h.push(e), j(this));
    }),
    (O.prototype.render = E),
    (k = []),
    (x = 0),
    (S =
      "function" == typeof Promise
        ? Promise.prototype.then.bind(Promise.resolve())
        : setTimeout),
    (M = N),
    (L = 0);
  var te,
    ne,
    re,
    oe = [],
    ie = C.__r,
    ae = C.diffed,
    se = C.__c,
    le = C.unmount;
  function ue(e) {
    C.__h && C.__h(ne);
    var t = ne.__H || (ne.__H = { __: [], __h: [] });
    return e >= t.__.length && t.__.push({}), t.__[e];
  }
  function ce(e, t, n) {
    var r = ue(te++);
    return (
      r.__c ||
        ((r.__c = ne),
        (r.__ = [
          n ? n(t) : be(void 0, t),
          function (t) {
            var n = e(r.__[0], t);
            r.__[0] !== n && ((r.__[0] = n), r.__c.setState({}));
          }
        ])),
      r.__
    );
  }
  function pe(e, t) {
    var n = ue(te++);
    ge(n.__H, t) && ((n.__ = e), (n.__H = t), ne.__h.push(n));
  }
  function fe(e, t) {
    var n = ue(te++);
    return ge(n.__H, t) ? ((n.__H = t), (n.__h = e), (n.__ = e())) : n.__;
  }
  function he() {
    oe.some(function (e) {
      if (e.__P)
        try {
          e.__H.__h.forEach(me), e.__H.__h.forEach(de), (e.__H.__h = []);
        } catch (t) {
          return C.__e(t, e.__v), !0;
        }
    }),
      (oe = []);
  }
  function me(e) {
    e.t && e.t();
  }
  function de(e) {
    var t = e.__();
    "function" == typeof t && (e.t = t);
  }
  function ge(e, t) {
    return (
      !e ||
      t.some(function (t, n) {
        return t !== e[n];
      })
    );
  }
  function be(e, t) {
    return "function" == typeof t ? t(e) : t;
  }
  function _e(e, t) {
    for (var n in t) e[n] = t[n];
    return e;
  }
  function ye(e, t) {
    for (var n in e) if ("__source" !== n && !(n in t)) return !0;
    for (var r in t) if ("__source" !== r && e[r] !== t[r]) return !0;
    return !1;
  }
  (C.__r = function (e) {
    ie && ie(e),
      (te = 0),
      (ne = e.__c).__H &&
        (ne.__H.__h.forEach(me), ne.__H.__h.forEach(de), (ne.__H.__h = []));
  }),
    (C.diffed = function (e) {
      ae && ae(e);
      var t = e.__c;
      if (t) {
        var n = t.__H;
        n &&
          n.__h.length &&
          ((1 !== oe.push(t) && re === C.requestAnimationFrame) ||
            (
              (re = C.requestAnimationFrame) ||
              function (e) {
                var t,
                  n = function () {
                    clearTimeout(r), cancelAnimationFrame(t), setTimeout(e);
                  },
                  r = setTimeout(n, 100);
                "undefined" != typeof window && (t = requestAnimationFrame(n));
              }
            )(he));
      }
    }),
    (C.__c = function (e, t) {
      t.some(function (e) {
        try {
          e.__h.forEach(me),
            (e.__h = e.__h.filter(function (e) {
              return !e.__ || de(e);
            }));
        } catch (n) {
          t.some(function (e) {
            e.__h && (e.__h = []);
          }),
            (t = []),
            C.__e(n, e.__v);
        }
      }),
        se && se(e, t);
    }),
    (C.unmount = function (e) {
      le && le(e);
      var t = e.__c;
      if (t) {
        var n = t.__H;
        if (n)
          try {
            n.__.forEach(function (e) {
              return e.t && e.t();
            });
          } catch (e) {
            C.__e(e, t.__v);
          }
      }
    });
  var ve = (function (e) {
    var t, n;
    function r(t) {
      var n;
      return ((n = e.call(this, t) || this).isPureReactComponent = !0), n;
    }
    return (
      (n = e),
      ((t = r).prototype = Object.create(n.prototype)),
      (t.prototype.constructor = t),
      (t.__proto__ = n),
      (r.prototype.shouldComponentUpdate = function (e, t) {
        return ye(this.props, e) || ye(this.state, t);
      }),
      r
    );
  })(O);
  var Ce = C.__b;
  C.__b = function (e) {
    e.type && e.type.t && e.ref && ((e.props.ref = e.ref), (e.ref = null)),
      Ce && Ce(e);
  };
  var ke = function (e, t) {
      return e
        ? H(e).reduce(function (e, n, r) {
            return e.concat(t(n, r));
          }, [])
        : null;
    },
    xe = {
      map: ke,
      forEach: ke,
      count: function (e) {
        return e ? H(e).length : 0;
      },
      only: function (e) {
        if (1 !== (e = H(e)).length)
          throw new Error("Children.only() expects only one child.");
        return e[0];
      },
      toArray: H
    },
    Se = C.__e;
  function we(e) {
    return (
      e && (((e = _e({}, e)).__c = null), (e.__k = e.__k && e.__k.map(we))), e
    );
  }
  function Me(e) {
    (this.__u = 0), (this.o = null), (this.__b = null);
  }
  function Le(e) {
    var t = e.__.__c;
    return t && t.u && t.u(e);
  }
  function Ne() {
    (this.i = null), (this.l = null);
  }
  (C.__e = function (e, t, n) {
    if (e.then)
      for (var r, o = t; (o = o.__); )
        if ((r = o.__c) && r.__c) return r.__c(e, t.__c);
    Se(e, t, n);
  }),
    ((Me.prototype = new O()).__c = function (e, t) {
      var n = this;
      null == n.o && (n.o = []), n.o.push(t);
      var r = Le(n.__v),
        o = !1,
        i = function () {
          o || ((o = !0), r ? r(a) : a());
        };
      (t.__c = t.componentWillUnmount),
        (t.componentWillUnmount = function () {
          i(), t.__c && t.__c();
        });
      var a = function () {
        var e;
        if (!--n.__u)
          for (
            n.__v.__k[0] = n.state.u, n.setState({ u: (n.__b = null) });
            (e = n.o.pop());

          )
            e.forceUpdate();
      };
      n.__u++ || n.setState({ u: (n.__b = n.__v.__k[0]) }), e.then(i, i);
    }),
    (Me.prototype.render = function (e, t) {
      return (
        this.__b && ((this.__v.__k[0] = we(this.__b)), (this.__b = null)),
        [z(O, null, t.u ? null : e.children), t.u && e.fallback]
      );
    });
  var qe = function (e, t, n) {
    if (
      (++n[1] === n[0] && e.l.delete(t),
      e.props.revealOrder && ("t" !== e.props.revealOrder[0] || !e.l.size))
    )
      for (n = e.i; n; ) {
        for (; n.length > 3; ) n.pop()();
        if (n[1] < n[0]) break;
        e.i = n = n[2];
      }
  };
  ((Ne.prototype = new O()).u = function (e) {
    var t = this,
      n = Le(t.__v),
      r = t.l.get(e);
    return (
      r[0]++,
      function (o) {
        var i = function () {
          t.props.revealOrder ? (r.push(o), qe(t, e, r)) : o();
        };
        n ? n(i) : i();
      }
    );
  }),
    (Ne.prototype.render = function (e) {
      (this.i = null), (this.l = new Map());
      var t = H(e.children);
      e.revealOrder && "b" === e.revealOrder[0] && t.reverse();
      for (var n = t.length; n--; ) this.l.set(t[n], (this.i = [1, 0, this.i]));
      return e.children;
    }),
    (Ne.prototype.componentDidUpdate = Ne.prototype.componentDidMount = function () {
      var e = this;
      e.l.forEach(function (t, n) {
        qe(e, n, t);
      });
    });
  var De = (function () {
    function e() {}
    var t = e.prototype;
    return (
      (t.getChildContext = function () {
        return this.props.context;
      }),
      (t.render = function (e) {
        return e.children;
      }),
      e
    );
  })();
  function Ie(e) {
    var t = this,
      n = e.container,
      r = z(De, { context: t.context }, e.vnode);
    return (
      t.s &&
        t.s !== n &&
        (t.v.parentNode && t.s.removeChild(t.v), X(t.h), (t.p = !1)),
      e.vnode
        ? t.p
          ? ((n.__k = t.__k), K(r, n), (t.__k = n.__k))
          : ((t.v = document.createTextNode("")),
            Q("", n),
            n.appendChild(t.v),
            (t.p = !0),
            (t.s = n),
            K(r, n, t.v),
            (t.__k = t.v.__k))
        : t.p && (t.v.parentNode && t.s.removeChild(t.v), X(t.h)),
      (t.h = r),
      (t.componentWillUnmount = function () {
        t.v.parentNode && t.s.removeChild(t.v), X(t.h);
      }),
      null
    );
  }
  var Te = /^(?:accent|alignment|arabic|baseline|cap|clip(?!PathU)|color|fill|flood|font|glyph(?!R)|horiz|marker(?!H|W|U)|overline|paint|stop|strikethrough|stroke|text(?!L)|underline|unicode|units|v|vector|vert|word|writing|x(?!C))[A-Z]/;
  O.prototype.isReactComponent = {};
  var ze =
    ("undefined" != typeof Symbol &&
      Symbol.for &&
      Symbol.for("react.element")) ||
    60103;
  function Fe(e, t, n) {
    if (null == t.__k) for (; t.firstChild; ) t.removeChild(t.firstChild);
    return K(e, t), "function" == typeof n && n(), e ? e.__c : null;
  }
  var Ee = C.event;
  function Oe(e, t) {
    e["UNSAFE_" + t] &&
      !e[t] &&
      Object.defineProperty(e, t, {
        configurable: !1,
        get: function () {
          return this["UNSAFE_" + t];
        },
        set: function (e) {
          this["UNSAFE_" + t] = e;
        }
      });
  }
  C.event = function (e) {
    Ee && (e = Ee(e)), (e.persist = function () {});
    var t = !1,
      n = !1,
      r = e.stopPropagation;
    e.stopPropagation = function () {
      r.call(e), (t = !0);
    };
    var o = e.preventDefault;
    return (
      (e.preventDefault = function () {
        o.call(e), (n = !0);
      }),
      (e.isPropagationStopped = function () {
        return t;
      }),
      (e.isDefaultPrevented = function () {
        return n;
      }),
      (e.nativeEvent = e)
    );
  };
  var Ve = {
      configurable: !0,
      get: function () {
        return this.class;
      }
    },
    Ae = C.vnode;
  function je(e) {
    return !!e && e.$$typeof === ze;
  }
  C.vnode = function (e) {
    e.$$typeof = ze;
    var t = e.type,
      n = e.props;
    if (
      (n.class != n.className &&
        ((Ve.enumerable = "className" in n),
        null != n.className && (n.class = n.className),
        Object.defineProperty(n, "className", Ve)),
      "function" != typeof t)
    ) {
      var r, o, i;
      for (i in (n.defaultValue &&
        void 0 !== n.value &&
        (n.value || 0 === n.value || (n.value = n.defaultValue),
        delete n.defaultValue),
      Array.isArray(n.value) &&
        n.multiple &&
        "select" === t &&
        (H(n.children).forEach(function (e) {
          -1 != n.value.indexOf(e.props.value) && (e.props.selected = !0);
        }),
        delete n.value),
      n))
        if ((r = Te.test(i))) break;
      if (r)
        for (i in ((o = e.props = {}), n))
          o[Te.test(i) ? i.replace(/([A-Z0-9])/, "-$1").toLowerCase() : i] =
            n[i];
    }
    !(function (t) {
      var n = e.type,
        r = e.props;
      if (r && "string" == typeof n) {
        var o = {};
        for (var i in r)
          /^on(Ani|Tra|Tou)/.test(i) &&
            ((r[i.toLowerCase()] = r[i]), delete r[i]),
            (o[i.toLowerCase()] = i);
        if (
          (o.ondoubleclick &&
            ((r.ondblclick = r[o.ondoubleclick]), delete r[o.ondoubleclick]),
          o.onbeforeinput &&
            ((r.onbeforeinput = r[o.onbeforeinput]), delete r[o.onbeforeinput]),
          o.onchange &&
            ("textarea" === n ||
              ("input" === n.toLowerCase() && !/^fil|che|ra/i.test(r.type))))
        ) {
          var a = o.oninput || "oninput";
          r[a] || ((r[a] = r[o.onchange]), delete r[o.onchange]);
        }
      }
    })(),
      "function" == typeof t &&
        !t.m &&
        t.prototype &&
        (Oe(t.prototype, "componentWillMount"),
        Oe(t.prototype, "componentWillReceiveProps"),
        Oe(t.prototype, "componentWillUpdate"),
        (t.m = !0)),
      Ae && Ae(e);
  };
  var Pe = {
    useState: function (e) {
      return ce(be, e);
    },
    useReducer: ce,
    useEffect: function (e, t) {
      var n = ue(te++);
      ge(n.__H, t) && ((n.__ = e), (n.__H = t), ne.__H.__h.push(n));
    },
    useLayoutEffect: pe,
    useRef: function (e) {
      return fe(function () {
        return { current: e };
      }, []);
    },
    useImperativeHandle: function (e, t, n) {
      pe(
        function () {
          "function" == typeof e ? e(t()) : e && (e.current = t());
        },
        null == n ? n : n.concat(e)
      );
    },
    useMemo: fe,
    useCallback: function (e, t) {
      return fe(function () {
        return e;
      }, t);
    },
    useContext: function (e) {
      var t = ne.context[e.__c];
      if (!t) return e.__;
      var n = ue(te++);
      return null == n.__ && ((n.__ = !0), t.sub(ne)), t.props.value;
    },
    useDebugValue: function (e, t) {
      C.useDebugValue && C.useDebugValue(t ? t(e) : e);
    },
    version: "16.8.0",
    Children: xe,
    render: Fe,
    hydrate: Fe,
    unmountComponentAtNode: function (e) {
      return !!e.__k && (K(null, e), !0);
    },
    createPortal: function (e, t) {
      return z(Ie, { vnode: e, container: t });
    },
    createElement: z,
    createContext: function (e) {
      var t = {},
        n = {
          __c: "__cC" + L++,
          __: e,
          Consumer: function (e, t) {
            return e.children(t);
          },
          Provider: function (e) {
            var r,
              o = this;
            return (
              this.getChildContext ||
                ((r = []),
                (this.getChildContext = function () {
                  return (t[n.__c] = o), t;
                }),
                (this.shouldComponentUpdate = function (t) {
                  e.value !== t.value &&
                    r.some(function (e) {
                      (e.context = t.value), j(e);
                    });
                }),
                (this.sub = function (e) {
                  r.push(e);
                  var t = e.componentWillUnmount;
                  e.componentWillUnmount = function () {
                    r.splice(r.indexOf(e), 1), t && t.call(e);
                  };
                })),
              e.children
            );
          }
        };
      return (n.Consumer.contextType = n), n;
    },
    createFactory: function (e) {
      return z.bind(null, e);
    },
    cloneElement: function (e) {
      return je(e) ? ee.apply(null, arguments) : e;
    },
    createRef: function () {
      return {};
    },
    Fragment: E,
    isValidElement: je,
    findDOMNode: function (e) {
      return (e && (e.base || (1 === e.nodeType && e))) || null;
    },
    Component: O,
    PureComponent: ve,
    memo: function (e, t) {
      function n(e) {
        var n = this.props.ref,
          r = n == e.ref;
        return (
          !r && n && (n.call ? n(null) : (n.current = null)),
          t ? !t(this.props, e) || !r : ye(this.props, e)
        );
      }
      function r(t) {
        return (this.shouldComponentUpdate = n), z(e, _e({}, t));
      }
      return (
        (r.prototype.isReactComponent = !0),
        (r.displayName = "Memo(" + (e.displayName || e.name) + ")"),
        (r.t = !0),
        r
      );
    },
    forwardRef: function (e) {
      function t(t) {
        var n = _e({}, t);
        return delete n.ref, e(n, t.ref);
      }
      return (
        (t.prototype.isReactComponent = t.t = !0),
        (t.displayName = "ForwardRef(" + (e.displayName || e.name) + ")"),
        t
      );
    },
    unstable_batchedUpdates: function (e, t) {
      return e(t);
    },
    Suspense: Me,
    SuspenseList: Ne,
    lazy: function (e) {
      var t, n, r;
      function o(o) {
        if (
          (t ||
            (t = e()).then(
              function (e) {
                n = e.default || e;
              },
              function (e) {
                r = e;
              }
            ),
          r)
        )
          throw r;
        if (!n) throw t;
        return z(n, o);
      }
      return (o.displayName = "Lazy"), (o.t = !0), o;
    }
  };
  function Re() {}
  function He() {}
  He.resetWarningCache = Re;
  var Ue = (function (e, t) {
    return e((t = { exports: {} }), t.exports), t.exports;
  })(function (e) {
    e.exports = (function () {
      function e(e, t, n, r, o, i) {
        if ("SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED" !== i) {
          var a = new Error(
            "Calling PropTypes validators directly is not supported by the `prop-types` package. Use PropTypes.checkPropTypes() to call them. Read more at http://fb.me/use-check-prop-types"
          );
          throw ((a.name = "Invariant Violation"), a);
        }
      }
      function t() {
        return e;
      }
      e.isRequired = e;
      var n = {
        array: e,
        bool: e,
        func: e,
        number: e,
        object: e,
        string: e,
        symbol: e,
        any: e,
        arrayOf: t,
        element: e,
        elementType: e,
        instanceOf: t,
        node: e,
        objectOf: t,
        oneOf: t,
        oneOfType: t,
        shape: t,
        exact: t,
        checkPropTypes: He,
        resetWarningCache: Re
      };
      return (n.PropTypes = n), n;
    })();
  });
  function Be() {
    return (Be =
      Object.assign ||
      function (e) {
        for (var t = 1; t < arguments.length; t++) {
          var n = arguments[t];
          for (var r in n)
            Object.prototype.hasOwnProperty.call(n, r) && (e[r] = n[r]);
        }
        return e;
      }).apply(this, arguments);
  }
  function We(e) {
    for (var t = 1; t < arguments.length; t++) {
      var n = null != arguments[t] ? arguments[t] : {},
        r = Object.keys(n);
      "function" == typeof Object.getOwnPropertySymbols &&
        (r = r.concat(
          Object.getOwnPropertySymbols(n).filter(function (e) {
            return Object.getOwnPropertyDescriptor(n, e).enumerable;
          })
        )),
        r.forEach(function (t) {
          Ze(e, t, n[t]);
        });
    }
    return e;
  }
  function Ze(e, t, n) {
    return (
      t in e
        ? Object.defineProperty(e, t, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
          })
        : (e[t] = n),
      e
    );
  }
  function $e(e, t) {
    if (null == e) return {};
    var n,
      r,
      o = (function (e, t) {
        if (null == e) return {};
        var n,
          r,
          o = {},
          i = Object.keys(e);
        for (r = 0; r < i.length; r++)
          (n = i[r]), t.indexOf(n) >= 0 || (o[n] = e[n]);
        return o;
      })(e, t);
    if (Object.getOwnPropertySymbols) {
      var i = Object.getOwnPropertySymbols(e);
      for (r = 0; r < i.length; r++)
        (n = i[r]),
          t.indexOf(n) >= 0 ||
            (Object.prototype.propertyIsEnumerable.call(e, n) && (o[n] = e[n]));
    }
    return o;
  }
  function Ye(e) {
    return "large" === e
      ? "20px"
      : "small" === e
      ? "12px"
      : "inherit" === e
      ? "null"
      : "16px";
  }
  var Ge = function (e) {
    var t = e.d,
      n = e.size,
      r = e.style,
      o = $e(e, ["d", "size", "style"]);
    return Pe.createElement(
      "i",
      Be(
        {
          style: We(
            {
              fontSize: Ye(n),
              display: "inline-block",
              fontStyle: "normal",
              lineHeight: "0",
              textAlign: "center",
              textTransform: "none",
              verticalAlign: "-.125em",
              textRendering: "optimizeLegibility",
              WebkitFontSmoothing: "antialiased",
              MozOsxFontSmoothing: "grayscale"
            },
            r
          )
        },
        o
      ),
      Pe.createElement(
        "svg",
        {
          xmlns: "http://www.w3.org/2000/svg",
          width: "1em",
          height: "1em",
          viewBox: "0 0 16 16",
          fill: "currentColor"
        },
        Pe.createElement("path", { d: t })
      )
    );
  };
  var Xe = {
    "lui-icon--tick": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          { d: "M6,10 L13,3 L15,5 L8,12 L6,14 L1,9 L3,7 L6,10 Z" },
          e
        )
      );
    },
    "lui-icon--star": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M15.5547828,6.35617376 L11.108898,9.91288163 C11.0434673,9.96522615 11.0176029,10.0528088 11.0441003,10.1323009 L12.7858364,15.3575093 C12.820766,15.462298 12.764134,15.575562 12.6593453,15.6104915 C12.5949847,15.6319451 12.5241365,15.6193092 12.4711608,15.5769286 L8.12493901,12.0999512 C8.05189528,12.0415162 7.94810472,12.0415162 7.87506099,12.0999512 L3.52883924,15.5769286 C3.44258685,15.6459305 3.31672838,15.6319462 3.24772647,15.5456939 C3.20534585,15.4927181 3.19271002,15.4218699 3.21416357,15.3575093 L4.95589969,10.1323009 C4.98239708,10.0528088 4.95653269,9.96522615 4.89110204,9.91288163 L0.445217202,6.35617376 C0.358964815,6.28717185 0.34498054,6.16131338 0.41398245,6.07506099 C0.451936887,6.02761794 0.509399467,6 0.570156212,6 L5.85584816,6 C5.94193424,6 6.01836201,5.94491398 6.04558482,5.86324555 L7.81026334,0.569209979 C7.84519289,0.464421315 7.95845689,0.407789318 8.06324555,0.442718872 C8.12296655,0.462625872 8.16982966,0.50948898 8.18973666,0.569209979 L9.95441518,5.86324555 C9.98163799,5.94491398 10.0580658,6 10.1441518,6 L15.4298438,6 C15.5403007,6 15.6298438,6.08954305 15.6298438,6.2 C15.6298438,6.26075674 15.6022258,6.31821932 15.5547828,6.35617376 Z"
          },
          e
        )
      );
    },
    "lui-icon--triangle-top": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M13.6,11 L2.4,11 C2.28954305,11 2.2,10.9104569 2.2,10.8 C2.2,10.756726 2.21403557,10.7146192 2.24,10.68 L7.84,3.21333333 C7.90627417,3.12496777 8.03163444,3.10705916 8.12,3.17333333 C8.13516113,3.18470418 8.14862915,3.1981722 8.16,3.21333333 L13.76,10.68 C13.8262742,10.7683656 13.8083656,10.8937258 13.72,10.96 C13.6853808,10.9859644 13.643274,11 13.6,11 Z"
          },
          e
        )
      );
    },
    "lui-icon--triangle-bottom": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M2.4,4 L13.6,4 C13.7104569,4 13.8,4.08954305 13.8,4.2 C13.8,4.24327404 13.7859644,4.28538077 13.76,4.32 L8.16,11.7866667 C8.09372583,11.8750322 7.96836556,11.8929408 7.88,11.8266667 C7.86483887,11.8152958 7.85137085,11.8018278 7.84,11.7866667 L2.24,4.32 C2.17372583,4.23163444 2.19163444,4.10627417 2.28,4.04 C2.31461923,4.01403557 2.35672596,4 2.4,4 Z"
          },
          e
        )
      );
    },
    "lui-icon--triangle-left": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M11,2.4 L11,13.6 C11,13.7104569 10.9104569,13.8 10.8,13.8 C10.756726,13.8 10.7146192,13.7859644 10.68,13.76 L3.21333333,8.16 C3.12496777,8.09372583 3.10705916,7.96836556 3.17333333,7.88 C3.18470418,7.86483887 3.1981722,7.85137085 3.21333333,7.84 L10.68,2.24 C10.7683656,2.17372583 10.8937258,2.19163444 10.96,2.28 C10.9859644,2.31461923 11,2.35672596 11,2.4 Z"
          },
          e
        )
      );
    },
    "lui-icon--triangle-right": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M4,13.6 L4,2.4 C4,2.28954305 4.08954305,2.2 4.2,2.2 C4.24327404,2.2 4.28538077,2.21403557 4.32,2.24 L11.7866667,7.84 C11.8750322,7.90627417 11.8929408,8.03163444 11.8266667,8.12 C11.8152958,8.13516113 11.8018278,8.14862915 11.7866667,8.16 L4.32,13.76 C4.23163444,13.8262742 4.10627417,13.8083656 4.04,13.72 C4.01403557,13.6853808 4,13.643274 4,13.6 Z"
          },
          e
        )
      );
    },
    "lui-icon--plus": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M9,9 L12.5,9 C12.75,9 13,8.7 13,8.4 L13,7.6 C13,7.3 12.75,7 12.5,7 L9,7 L9,3.5 C9,3.25 8.7,3 8.4,3 L7.7,3 C7.3,3 7,3.25 7,3.5 L7,7 L3.5,7 C3.25,7 3,7.3 3,7.6 L3,8.3 C3,8.7 3.25,9 3.5,9 L7,9 L7,12.5 C7,12.75 7.3,13 7.6,13 L8.4,13 C8.7,13 9,12.75 9,12.5 L9,9 Z M8,0 C12.4,0 16,3.6 16,8 C16,12.4 12.4,16 8,16 C3.6,16 0,12.4 0,8 C0,3.6 3.6,0 8,0 Z"
          },
          e
        )
      );
    },
    "lui-icon--minus": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M8,0 C12.4,0 16,3.6 16,8 C16,12.4 12.4,16 8,16 C3.6,16 0,12.4 0,8 C0,3.6 3.6,0 8,0 Z M12.5,9 C12.75,9 13,8.7 13,8.4 L13,7.6 C13,7.3 12.75,7 12.5,7 L3.5,7 C3.25,7 3,7.3 3,7.6 L3,8.3 C3,8.7 3.25,9 3.5,9 L12.5,9 Z"
          },
          e
        )
      );
    },
    "lui-icon--warning-triangle": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M15.7375198,13.5 C16.2375198,14.3 15.8375198,15 14.9375198,15 L1.03751982,15 C0.0375198248,15 -0.262480175,14.3 0.237519825,13.5 L7.03751982,1.6 C7.53751982,0.8 8.33751982,0.8 8.83751982,1.6 L15.7375198,13.5 Z M8.93751982,11.5 C8.93751982,11.2 8.73751982,11 8.43751982,11 L7.43751982,11 C7.13751982,11 6.93751982,11.2 6.93751982,11.5 L6.93751982,12.5 C6.93751982,12.8 7.13751982,13 7.43751982,13 L8.43751982,13 C8.73751982,13 8.93751982,12.8 8.93751982,12.5 L8.93751982,11.5 Z M8.93751982,5.5 C8.93751982,5.2 8.73751982,5 8.43751982,5 L7.43751982,5 C7.13751982,5 6.93751982,5.2 6.93751982,5.5 L6.93751982,9.5 C6.93751982,9.8 7.13751982,10 7.43751982,10 L8.43751982,10 C8.73751982,10 8.93751982,9.8 8.93751982,9.5 L8.93751982,5.5 Z"
          },
          e
        )
      );
    },
    "lui-icon--hand": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M13.9,7.4 C12.9,8.7 13.3,8.9 12.4,11.5 C11.9,13.3 11.2,14.4 10.7,15.1 C10.4,15.5 10.1,15.8 9.8,16 L4.2,16 C3.2,16 2.6,15.5 2.3,15 C2.1,14.7 2,14.4 2,14 L2,4.2 C2,3.8 2.3,3.5 2.7,3.5 C3.1,3.5 3.4,3.8 3.4,4.2 L3.4,7.8 L4.3,7.7 L4.3,1.6 C4.3,1.2 4.6,0.9 5,0.9 C5.4,0.9 5.7,1.2 5.7,1.6 L5.7,6.7 L6.8,6.5 L6.8,0.7 C6.7,0.3 7,0 7.4,0 C7.8,0 8.1,0.3 8.1,0.7 L8.1,6.6 L9.2,6.7 L9.2,1.6 C9.2,1.2 9.5,0.9 9.9,0.9 C10.3,0.9 10.6,1.2 10.6,1.6 L10.6,9.3 C10.6,9.3 10.6,9.3 10.6,9.3 C10.6,9.3 10.7,9.3 10.7,9.1 L11.3,7.7 C12.4,5.6 14.4,6.6 13.9,7.4 Z"
          },
          e
        )
      );
    },
    "lui-icon--flag": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M3,9 L3,15.5 C3,15.7761424 2.77614237,16 2.5,16 C2.22385763,16 2,15.7761424 2,15.5 L2,0.5 C2,0.223857625 2.22385763,5.07265313e-17 2.5,0 C2.77614237,-5.07265313e-17 3,0.223857625 3,0.5 L3,1 C4.62743495,1.42599508 5.84965717,1.63899261 6.66666667,1.63899261 C7.89218092,1.63899261 8.44059821,0.64422826 10.5376148,0.443223427 C11.8517998,0.317255166 12.9504722,0.473704875 13.8336319,0.912572557 L13.8335748,0.912687425 C13.9355292,0.96335152 14,1.06738112 14,1.18122992 L14.0001528,8.5254774 C14.0001528,8.69124724 13.8657698,8.82563025 13.7,8.82563025 C13.6598942,8.82563025 13.6201952,8.81759278 13.5832478,8.8019926 C12.6911212,8.42531264 11.675962,8.28656104 10.5377701,8.38573779 C8.55866392,8.55818792 8.45466035,9.4932868 6.66666667,9.63899261 C5.47467088,9.73612983 4.25244866,9.52313229 3,9 Z"
          },
          e
        )
      );
    },
    "lui-icon--lightbulb": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M5,13 L11,13 L11,14.5 C11,14.8 10.8,15 10.5,15 L10,15 L10,15.5 C10,15.8 9.8,16 9.5,16 L6.5,16 C6.2,16 6,15.8 6,15.5 L6,15 L5.5,15 C5.2,15 5,14.8 5,14.5 L5,13 Z M5,12 C5,10 2,9 2,5 C2,2 5,0 8,0 C11,0 14,2 14,5 C14,9 11,9 11,12 L8,12 L5,12 Z M4.5,5 C4.8,5 5,4.8 5,4.5 C5,4.2 5.2,3.8 5.5,3.5 C5.8,3.2 6.1,3 6.5,3 C6.8,3 7,2.8 7,2.5 C7,2.2 6.8,2 6.5,2 C5.9,2 5.3,2.3 4.8,2.8 C4.3,3.3 4,3.9 4,4.5 C4,4.8 4.2,5 4.5,5 Z"
          },
          e
        )
      );
    },
    "lui-icon--stop": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M13,14 L3,14 C2.44771525,14 2,13.5522847 2,13 L2,3 C2,2.44771525 2.44771525,2 3,2 L13,2 C13.5522847,2 14,2.44771525 14,3 L14,13 C14,13.5522847 13.5522847,14 13,14 Z"
          },
          e
        )
      );
    },
    "lui-icon--pie-chart": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M7,1 L7,8 L14,8 C14,11.8659932 10.8659932,15 7,15 C3.13400675,15 0,11.8659932 0,8 C0,4.13400675 3.13400675,1 7,1 Z M8,1.15888847e-31 C11.8659932,6.95889291e-16 15,3.13400675 15,7 L8,7 L8,-8.8817842e-16 Z"
          },
          e
        )
      );
    },
    "lui-icon--add": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M6.5,6.5 L6.5,2 C6.5,1.44771525 6.94771525,1 7.5,1 L8.5,1 C9.05228475,1 9.5,1.44771525 9.5,2 L9.5,6.5 L14,6.5 C14.5522847,6.5 15,6.94771525 15,7.5 L15,8.5 C15,9.05228475 14.5522847,9.5 14,9.5 L9.5,9.5 L9.5,14 C9.5,14.5522847 9.05228475,15 8.5,15 L7.5,15 C6.94771525,15 6.5,14.5522847 6.5,14 L6.5,9.5 L2,9.5 C1.44771525,9.5 1,9.05228475 1,8.5 L1,7.5 C1,6.94771525 1.44771525,6.5 2,6.5 L6.5,6.5 Z"
          },
          e
        )
      );
    },
    "lui-icon--minus-2": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M2,6.5 L14,6.5 C14.5522847,6.5 15,6.94771525 15,7.5 L15,8.5 C15,9.05228475 14.5522847,9.5 14,9.5 L2,9.5 C1.44771525,9.5 1,9.05228475 1,8.5 L1,7.5 C1,6.94771525 1.44771525,6.5 2,6.5 Z"
          },
          e
        )
      );
    },
    "lui-icon--dot": function (e) {
      return Pe.createElement(
        Ge,
        Object.assign(
          {
            d:
              "M8,0 C12.4,0 16,3.6 16,8 C16,12.4 12.4,16 8,16 C3.6,16 0,12.4 0,8 C0,3.6 3.6,0 8,0 L8,0 Z"
          },
          e
        )
      );
    }
  };
  function Je(e) {
    var t = e.cssClass,
      n = Xe[t];
    return Pe.createElement(n, { size: "inherit", class: "value-icon" });
  }
  var Ke = Object.prototype.hasOwnProperty,
    Qe = Object.prototype.toString,
    et = Object.defineProperty,
    tt = Object.getOwnPropertyDescriptor,
    nt = function (e) {
      return "function" == typeof Array.isArray
        ? Array.isArray(e)
        : "[object Array]" === Qe.call(e);
    },
    rt = function (e) {
      if (!e || "[object Object]" !== Qe.call(e)) return !1;
      var t,
        n = Ke.call(e, "constructor"),
        r =
          e.constructor &&
          e.constructor.prototype &&
          Ke.call(e.constructor.prototype, "isPrototypeOf");
      if (e.constructor && !n && !r) return !1;
      for (t in e);
      return void 0 === t || Ke.call(e, t);
    },
    ot = function (e, t) {
      et && "__proto__" === t.name
        ? et(e, t.name, {
            enumerable: !0,
            configurable: !0,
            value: t.newValue,
            writable: !0
          })
        : (e[t.name] = t.newValue);
    },
    it = function (e, t) {
      if ("__proto__" === t) {
        if (!Ke.call(e, t)) return;
        if (tt) return tt(e, t).value;
      }
      return e[t];
    },
    at = function e() {
      var t,
        n,
        r,
        o,
        i,
        a,
        s = arguments[0],
        l = 1,
        u = arguments.length,
        c = !1;
      for (
        "boolean" == typeof s && ((c = s), (s = arguments[1] || {}), (l = 2)),
          (null == s || ("object" != typeof s && "function" != typeof s)) &&
            (s = {});
        l < u;
        ++l
      )
        if (null != (t = arguments[l]))
          for (n in t)
            (r = it(s, n)),
              s !== (o = it(t, n)) &&
                (c && o && (rt(o) || (i = nt(o)))
                  ? (i
                      ? ((i = !1), (a = r && nt(r) ? r : []))
                      : (a = r && rt(r) ? r : {}),
                    ot(s, { name: n, newValue: e(c, a, o) }))
                  : void 0 !== o && ot(s, { name: n, newValue: o }));
      return s;
    };
  /**
   * @preserve IntegraXor Web SCADA - JavaScript Number Formatter
   * http://www.integraxor.com/
   * author: KPL, KHL
   * (c)2011 ecava
   * Dual licensed under the MIT or GPL Version 2 licenses.
   */
  function st(e, t) {
    if (!e || isNaN(+t)) return t;
    var n = (t = "-" == e.charAt(0) ? -t : +t) < 0 ? (t = -t) : 0,
      r = e.match(/[^\d\-\+#]/g),
      o = (r && r[r.length - 1]) || ".",
      i = (r && r[1] && r[0]) || ",";
    e = e.split(o);
    (t = t.toFixed(e[1] && e[1].length)), (t = "".concat(+t));
    var a = e[1] && e[1].lastIndexOf("0"),
      s = t.split(".");
    (!s[1] || (s[1] && s[1].length <= a)) && (t = (+t).toFixed(a + 1));
    var l = e[0].split(i);
    e[0] = l.join("");
    var u = e[0] && e[0].indexOf("0");
    if (u > -1) for (; s[0].length < e[0].length - u; ) s[0] = "0".concat(s[0]);
    else 0 == +s[0] && (s[0] = "");
    (t = t.split("."))[0] = s[0];
    var c = l[1] && l[l.length - 1].length;
    if (c) {
      for (
        var p = t[0], f = "", h = p.length % c, m = 0, d = p.length;
        m < d;
        m++
      )
        (f += p.charAt(m)), !((m - h + 1) % c) && m < d - c && (f += i);
      t[0] = f;
    }
    return (t[1] = e[1] && t[1] ? o + t[1] : ""), (n ? "-" : "") + t[0] + t[1];
  }
  var lt = function (e) {
      return e.replace(/[-[\]/{}()*+?.\\^$|]/g, "\\$&");
    },
    ut = {
      3: "k",
      6: "M",
      9: "G",
      12: "T",
      15: "P",
      18: "E",
      21: "Z",
      24: "Y",
      "-3": "m",
      "-6": "μ",
      "-9": "n",
      "-12": "p",
      "-15": "f",
      "-18": "a",
      "-21": "z",
      "-24": "y"
    },
    ct = /%$/,
    pt = /^\(r(0[2-9]|[12]\d|3[0-6])\)/i,
    ft = /^\(oct\)/i,
    ht = /^\(dec\)/i,
    mt = /^\(hex\)/i,
    dt = /^\(bin\)/i,
    gt = /^\(rom\)/i,
    bt = /^(\(rom\)|\(bin\)|\(hex\)|\(dec\)|\(oct\)|\(r(0[2-9]|[12]\d|3[0-6])\))/i;
  function _t(e, t, n, r) {
    return (
      (e = e.toString(t)),
      n[1] === n[1].toUpperCase() && (e = e.toUpperCase()),
      e.length - e.indexOf(".") > 10 && (e = e.slice(0, e.indexOf(".") + 11)),
      e.replace(".", r || ".")
    );
  }
  function yt(e, t, n) {
    var r;
    return (
      pt.test(t)
        ? (e = _t(e, Number(/\d{2}/.exec(t)[0]), t, n))
        : ft.test(t)
        ? (e = _t(e, 8, t, n))
        : ht.test(t)
        ? (e = _t(e, 10, t, n))
        : mt.test(t)
        ? (e = _t(e, 16, t, n))
        : dt.test(t)
        ? (e = _t(e, 2, t, n))
        : gt.test(t) &&
          ((r = ""),
          e < 0 && ((r = "-"), (e = -e)),
          (e =
            0 === (e = Math.floor(e))
              ? "0"
              : e <= 5e5
              ? r +
                (e = (function (e, t) {
                  var n,
                    r = "",
                    o = Number(String(e).slice(-3)),
                    i = (e - o) / 1e3,
                    a = [
                      0,
                      1,
                      4,
                      5,
                      9,
                      10,
                      40,
                      50,
                      90,
                      100,
                      400,
                      500,
                      900
                    ].reverse(),
                    s = [
                      "0",
                      "I",
                      "IV",
                      "V",
                      "IX",
                      "X",
                      "XL",
                      "L",
                      "XC",
                      "C",
                      "CD",
                      "D",
                      "CM"
                    ].reverse();
                  for (; o > 0; )
                    for (n = 0; n < a.length; n++)
                      if (a[n] <= o) {
                        (r += s[n]), (o -= a[n]);
                        break;
                      }
                  for (n = 0; n < i; n++) r = "M".concat(r);
                  t[1] !== t[1].toUpperCase() && (r = r.toLowerCase());
                  return r;
                })(e, t))
              : t + r + e.toExponential(0))),
      e
    );
  }
  var vt = (function () {
    function e(t, r, o, i, a) {
      n(this, e),
        (this.localeInfo = t),
        (this.pattern = r),
        (this.thousandDelimiter = o || ","),
        (this.decimalDelimiter = i || "."),
        (this.type = a || "numeric"),
        (this.abbreviations = (function (e) {
          if (!e || !e.qNumericalAbbreviation) return ut;
          var t = {};
          return (
            e.qNumericalAbbreviation.split(";").forEach(function (e) {
              var n = e.split(":");
              2 === n.length && (t[n[0]] = n[1]);
            }),
            t
          );
        })(t)),
        this.prepare();
    }
    return (
      o(e, [
        {
          key: "clone",
          value: function () {
            var t = new e(
              this.localeInfo,
              this.pattern,
              this.thousandDelimiter,
              this.decimalDelimiter,
              this.type
            );
            return (t.subtype = this.subtype), t;
          }
        },
        {
          key: "format",
          value: function (e, t, n, r) {
            return this.prepare(t, n, r), this.formatValue(e);
          }
        },
        {
          key: "prepare",
          value: function (e, t, n) {
            if (
              (void 0 === e && (e = this.pattern),
              void 0 === t && (t = this.thousandDelimiter),
              void 0 === n && (n = this.decimalDelimiter),
              e)
            ) {
              this._prepared = {
                positive: {
                  d: n,
                  t: t,
                  abbreviate: !1,
                  isFunctional: !1,
                  prefix: "",
                  postfix: ""
                },
                negative: {
                  d: n,
                  t: t,
                  abbreviate: !1,
                  isFunctional: !1,
                  prefix: "",
                  postfix: ""
                }
              };
              var r = this._prepared;
              (e = e.split(";")),
                (r.positive.pattern = e[0]),
                (r.negative.pattern = e[1]),
                bt.test(e[0]) && (r.positive.isFunctional = !0),
                e[1]
                  ? bt.test(e[1]) && (r.negative.isFunctional = !0)
                  : (r.negative = !1),
                (r.positive.isFunctional &&
                  (!r.negative || (r.negative && r.negative.isFunctional))) ||
                  (Ct(r.positive, t, n), r.negative && Ct(r.negative, t, n));
            } else this._prepared = { pattern: !1 };
          }
        },
        {
          key: "formatValue",
          value: function (e) {
            var t,
              n,
              r,
              o,
              i,
              a,
              s,
              l = this._prepared,
              u = "",
              c = "";
            if (Number.isNaN(+e)) return e;
            if (!1 === l.pattern) return e.toString();
            e < 0 && l.negative
              ? ((l = l.negative), (e = -e))
              : (l = l.positive);
            var p = l.d,
              f = l.t;
            if (l.isFunctional) e = yt(e, l.pattern, p);
            else {
              if ((l.percentage && (e *= 100), l.abbreviate)) {
                var h,
                  m,
                  d = this.abbreviations,
                  g = Object.keys(d)
                    .map(function (e) {
                      return parseInt(e, 10);
                    })
                    .sort(function (e, t) {
                      return e > t ? 1 : -1;
                    }),
                  b = g[0];
                for (
                  i = 0, n = Number(Number(e).toExponential().split("e")[1]);
                  b <= n && i < g.length;

                )
                  b = g[++i];
                i > 0 && (h = g[i - 1]),
                  h && n > 0 && h > 0
                    ? (m = h)
                    : ((n < 0 && h < 0) || !h) &&
                      (b < 0 && b - n <= l.maxPrecision
                        ? (m = b)
                        : h <= n &&
                          !(b > 0 && -n <= l.maxPrecision) &&
                          (m = h)),
                  m && ((u = d[m]), (e /= Math.pow(10, m)));
              }
              if (
                ((r = Math.abs(e)),
                (t = l.temp),
                (s = (a = l.numericPattern).split(p)[1]),
                "I" === this.type && (e = Math.round(e)),
                (o = e),
                s || "#" !== a.slice(-1)[0])
              )
                if (r >= 1e15 || (r > 0 && r <= 1e-14))
                  e = r ? r.toExponential(15).replace(/\.?0+(?=e)/, "") : "0";
                else {
                  var _ = Number(
                      e.toFixed(Math.min(20, s ? s.length : 0)).split(".")[0]
                    ),
                    y = a.split(p)[0];
                  if (((e = st((y += p), _) || "0"), s)) {
                    var v = Math.max(0, Math.min(14, s.length)),
                      C = s.replace(/#+$/, "").length,
                      k = Number((r % 1).toPrecision(v)).toFixed(v);
                    for (
                      i = (k = k.slice(2).replace(/0+$/, "")).length;
                      i < C;
                      i++
                    )
                      k += "0";
                    k && (e += p + k);
                  } else 0 === _ && (o = 0);
                }
              else if (r >= Math.pow(10, t) || r < 1 || r < 1e-4)
                0 === e
                  ? (e = "0")
                  : r < 1e-4 || r >= 1e20
                  ? ((e = (e = o.toExponential(
                      Math.max(1, Math.min(14, t)) - 1
                    )).replace(/\.?0+(?=e)/, "")),
                    (c = ""))
                  : (e = e.toPrecision(Math.max(1, Math.min(14, t)))).indexOf(
                      "."
                    ) >= 0 &&
                    (e = (e = e.replace(
                      e.indexOf("e") < 0 ? /0+$/ : /\.?0+(?=e)/,
                      ""
                    )).replace(".", p));
              else {
                for (
                  a += p,
                    t = Math.max(
                      0,
                      Math.min(20, t - Math.ceil(Math.log(r) / Math.log(10)))
                    ),
                    i = 0;
                  i < t;
                  i++
                )
                  a += "#";
                e = st(a, e);
              }
              (e = e.replace(l.numericRegex, function (e) {
                return e === f ? l.groupTemp : e === p ? l.decTemp : "";
              })),
                o < 0 && !/^-/.test(e) && (e = "-".concat(e));
            }
            return l.prefix + e + c + u + l.postfix;
          }
        }
      ]),
      e
    );
  })();
  function Ct(e, t, n) {
    var r,
      o,
      i = e.pattern;
    i.indexOf("A") >= 0 && ((i = i.replace("A", "")), (e.abbreviate = !0));
    var a = (function (e, t) {
        t && (t = lt(t));
        e && (e = lt(e));
        return new RegExp(
          "(?:[#0]+".concat(e, ")?[#0]+(?:").concat(t, "[#0]+)?")
        );
      })(t, n),
      s = i.match(a),
      l = (s = s ? s[0] : "") ? i.substr(0, i.indexOf(s)) : i,
      u = s ? i.substring(i.indexOf(s) + s.length) : "";
    s || (s = i ? "#" : "##########"),
      t &&
        t === n &&
        ((o = (r = s.split(n)).pop()), (s = r.join("") + n + o), (t = ""));
    var c = t;
    (t = /,/.test(n) ? "¤" : ","), c && (s = s.replace(kt(c, "g"), t));
    var p = n;
    (n = "."), p && (s = s.replace(kt(p, "g"), n));
    var f = s.match(/#/g);
    f = f ? f.length : 0;
    var h,
      m = i.split(p);
    m[1] && (h = m[1].match(/#|0/g)),
      (e.prefix = l || ""),
      (e.postfix = u || ""),
      (e.pattern = i),
      (e.maxPrecision = h ? h.length : 2),
      (e.percentage = ct.test(i)),
      (e.numericPattern = s || ""),
      (e.numericRegex = new RegExp(
        "".concat(kt(t, null, !0), "|").concat(kt(n, null, !0)),
        "g"
      )),
      (e.groupTemp = c),
      (e.decTemp = p),
      (e.t = t),
      (e.d = n),
      (e.temp = f);
  }
  function kt(e, t, n) {
    var r = lt(e);
    return n ? r : new RegExp(r || "", t);
  }
  vt.getStaticFormatter = function () {
    return {
      prepare: function () {},
      formatValue: function (e) {
        return "".concat(e);
      }
    };
  };
  var xt = [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ],
    St = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
    wt = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ],
    Mt = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];
  function Lt(e, t) {
    for (var n = e.length; n < t; n++) e = "0".concat(e);
    return e;
  }
  function Nt(e) {
    var t = e.toString().split(".");
    return t[1] ? (t = Number("0.".concat(t[1]))) : 0;
  }
  function qt(e) {
    var t = e,
      n = 24 * Nt(t),
      r = 60 * Nt(n),
      o = 60 * Nt(r),
      i = 1e3 * Nt(o);
    return {
      d: Math.floor(t),
      h: Math.floor(n),
      m: Math.floor(r),
      s: Math.floor(o),
      f: Math.round(i)
    };
  }
  var Dt = (function () {
    function e(t, r) {
      n(this, e);
      var o = t || {};
      o.qCalendarStrings ||
        (o.qCalendarStrings = {
          qLongDayNames: xt,
          qDayNames: St,
          qLongMonthNames: wt,
          qMonthNames: Mt
        }),
        (this.localeInfo = o),
        (this.locale_days = o.qCalendarStrings.qLongDayNames.slice()),
        (this.locale_days_abbr = o.qCalendarStrings.qDayNames.slice()),
        (this.locale_months = o.qCalendarStrings.qLongMonthNames.slice()),
        (this.locale_months_abbr = o.qCalendarStrings.qMonthNames.slice()),
        (this.pattern = r);
    }
    return (
      o(e, [
        {
          key: "clone",
          value: function () {
            var t = new e(this.localeInfo, this.pattern);
            return (t.subtype = this.subtype), t;
          }
        },
        {
          key: "format",
          value: function (e, t) {
            t || (t = this.pattern ? this.pattern : "YYYY-MM-DD hh:mm:ss"),
              (t = t.replace(/\[.+\]|\[|\]/g, ""));
            var n,
              r = /t+/gi.test(t);
            e instanceof Date
              ? (n = (function (e, t) {
                  var n = e.getHours(),
                    r = e.getDay() - 1;
                  return (
                    t && ((n %= 12) || (n = 12)),
                    r < 0 && (r = 6),
                    {
                      year: e.getFullYear(),
                      month: e.getMonth(),
                      day: r,
                      date: e.getDate(),
                      h: n,
                      m: e.getMinutes(),
                      s: e.getSeconds(),
                      f: e.getMilliseconds(),
                      t: e.getHours() >= 12 ? "pm" : "am"
                    }
                  );
                })(e, r))
              : (e < 0 && ((e = -e), (t = "-".concat(t))),
                (n = (function (e, t) {
                  var n,
                    r = qt(e),
                    o = r.d,
                    i = r.h,
                    a = r.m,
                    s = r.s,
                    l = r.f,
                    u = 0;
                  return (
                    /w+|t+/gi.test(t) &&
                      ((n = new Date(
                        1899,
                        11,
                        30 + Math.floor(e),
                        0,
                        0,
                        86400 * (e - Math.floor(e))
                      )),
                      Number.isNaN(+n.getTime()) && (n = null)),
                    /D+/gi.test(t) || (i += 24 * o),
                    /h+/gi.test(t) || (a += 60 * i),
                    /m+/gi.test(t) || (s += 60 * a),
                    /w+/gi.test(t) &&
                      (u = n ? n.getDay() - 1 : 0) < 0 &&
                      (u = 6),
                    {
                      year: 0,
                      month: 0,
                      day: u,
                      date: o,
                      h: i,
                      m: a,
                      s: s,
                      f: l,
                      t: n ? (n.getHours() >= 12 ? "pm" : "am") : ""
                    }
                  );
                })(e, t)));
            var o = (function (e, t) {
                return {
                  "Y+|y+": {
                    Y: "".concat(Number("".concat(t.year).slice(-2))),
                    YY: Lt("".concat(t.year).slice(-2), 2),
                    YYY: Lt("".concat(t.year).slice(-3), 3),
                    def: function (e) {
                      return Lt("".concat(t.year), e.length);
                    }
                  },
                  "M+": {
                    M: t.month + 1,
                    MM: Lt("".concat(t.month + 1), 2),
                    MMM: e.locale_months_abbr[t.month],
                    def: e.locale_months[t.month]
                  },
                  "W+|w+": {
                    W: t.day,
                    WW: Lt("".concat(t.day), 2),
                    WWW: e.locale_days_abbr[t.day],
                    def: e.locale_days[t.day]
                  },
                  "D+|d+": {
                    D: t.date,
                    def: function (e) {
                      return Lt("".concat(t.date), e.length);
                    }
                  },
                  "h+|H+": {
                    h: t.h,
                    def: function (e) {
                      return Lt("".concat(t.h), e.length);
                    }
                  },
                  "m+": {
                    m: t.m,
                    def: function (e) {
                      return Lt("".concat(t.m), e.length);
                    }
                  },
                  "s+|S+": {
                    s: t.s,
                    def: function (e) {
                      return Lt("".concat(t.s), e.length);
                    }
                  },
                  "f+|F+": {
                    def: function (e) {
                      var n = "".concat(t.f),
                        r = e.length - n.length;
                      if (r > 0) for (var o = 0; o < r; o++) n += "0";
                      else r < 0 && (n = n.slice(0, e.length));
                      return n;
                    }
                  },
                  "t{1,2}|T{1,2}": {
                    def: function (e) {
                      var n = t.t;
                      return (
                        e[0].toUpperCase() === e[0] && (n = n.toUpperCase()),
                        (n = n.slice(0, e.length))
                      );
                    }
                  }
                };
              })(this, n),
              i = [];
            for (var a in o) i.push(a);
            var s = new RegExp(i.join("|"), "g");
            return t.replace(s, function (e) {
              var t, n, r;
              for (n in o) if ((t = new RegExp(n)).test(e)) break;
              if (!t) return "";
              for (var i in o[n])
                if (i === e || i.toLowerCase() === e) {
                  void 0 === (r = o[n][i]) && (r = o[n][i.toLowerCase()]);
                  break;
                }
              return (
                void 0 === r && (r = o[n].def),
                "function" == typeof r && (r = r(e)),
                r
              );
            });
          }
        }
      ]),
      e
    );
  })();
  Dt.parseInterval = qt;
  var It = "U",
    Tt = "I",
    zt = "R",
    Ft = "F",
    Et = "M",
    Ot = "D",
    Vt = "T",
    At = "TS",
    jt = "IV",
    Pt = (function () {
      function e(t, r, o, i, a) {
        n(this, e);
        var s = t || {};
        (this.localeInfo = s),
          (this.pattern = r || ""),
          (this.decimalDelimiter =
            "string" == typeof i
              ? i
              : "string" == typeof s.qDecimalSep
              ? s.qDecimalSep
              : "."),
          (this.thousandDelimiter =
            "string" == typeof o
              ? o
              : "string" == typeof s.qThousandSep &&
                this.decimalDelimiter !== s.qThousandSep
              ? s.qThousandSep
              : ""),
          (this.type = a || It),
          a === It && (this.pattern = "#".concat(this.decimalDelimiter, "##A")),
          (this._numberFormatter = new vt(t)),
          (this._dateFormatter = new Dt(t)),
          this.prepare();
      }
      return (
        o(e, [
          {
            key: "clone",
            value: function () {
              return new e(
                this.localeInfo,
                this.pattern,
                this.thousandDelimiter,
                this.decimalDelimiter,
                this.type
              );
            }
          },
          {
            key: "prepare",
            value: function (e, t, n) {
              if ((e || (e = this.pattern), !e))
                switch (this.type) {
                  case Tt:
                    e = "##############";
                    break;
                  case zt:
                    e = Array(this.localeInfo.qnDec || 11).join("#");
                    break;
                  case Ft:
                    e =
                      "#".concat(this.localeInfo.qDecimalSep) ||
                      ".".concat(Array(this.localeInfo.qnDec || 4).join("#"));
                    break;
                  case Et:
                    (e = this.localeInfo.qMoneyFmt),
                      n || (n = this.localeInfo.qMoneyDecimalSep || "."),
                      t ||
                        n === this.localeInfo.qMoneyThousandSep ||
                        (t = this.localeInfo.qMoneyThousandSep);
                    break;
                  case Ot:
                    e = this.localeInfo.qDateFmt || "YYYY-MM-DD";
                    break;
                  case At:
                    e = this.localeInfo.qTimestampFmt || "YYYY-MM-DD hh:mm:ss";
                    break;
                  case Vt:
                  case jt:
                    e = this.localeInfo.qTimeFmt || "hh:mm:ss";
                    break;
                  default:
                    e = "##########";
                }
              (this._numberFormatter.pattern = this.pattern),
                (this._dateFormatter.pattern = this.pattern),
                (this._numberFormatter.thousandDelimiter = this.thousandDelimiter),
                (this._numberFormatter.decimalDelimiter = this.decimalDelimiter),
                (this._numberFormatter.type = this.type),
                this._numberFormatter.prepare(e, t, n),
                (this._prepared = { pattern: e, t: t, d: n });
            }
          },
          {
            key: "formatValue",
            value: function (e, t) {
              var n,
                r = this._prepared;
              if ("NaN" === e || ("number" == typeof e && Number.isNaN(+e)))
                return "-";
              if (Number.isNaN(+e) || "number" != typeof e) return "".concat(e);
              switch (this.type) {
                case Ot:
                case Vt:
                case At:
                  return (
                    (n = new Date(
                      1899,
                      11,
                      30 + Math.floor(e),
                      0,
                      0,
                      86400 * (e - Math.floor(e))
                    )),
                    Number.isNaN(+n.getTime())
                      ? this._numberFormatter.format(e, "0", r.t, r.d)
                      : this._dateFormatter.format(n, t || r.pattern)
                  );
                case jt:
                  return this._dateFormatter.format(e, r.pattern);
                default:
                  return this._numberFormatter.formatValue(e);
              }
            }
          },
          {
            key: "format",
            value: function (e, t, n, r) {
              return "NaN" === e || ("number" == typeof e && Number.isNaN(+e))
                ? "-"
                : Number.isNaN(+e) || "number" != typeof e
                ? "".concat(e)
                : (this.prepare(t, n, r), this.formatValue(e));
            }
          },
          {
            key: "createPatternFromRange",
            value: function (e, t, n) {
              var r,
                o,
                i = "",
                a = Number(Number(e).toExponential().split("e")[1]),
                s = Number(Number(t).toExponential().split("e")[1]),
                l = Math.min(Math.abs(a), Math.abs(s)),
                u = Math.abs(t - e),
                c = Number(
                  Number(u / 50)
                    .toExponential()
                    .split("e")[1]
                );
              if (((o = Math.abs(c)), 0 === u))
                return "0"
                  .concat(this.decimalDelimiter, "##")
                  .concat(n ? "A" : "");
              if (
                (c >= 0
                  ? (o = n ? Math.max(2, l - c) : 0)
                  : (o -= (n ? l - (l % 3) : 0) * (s < 0 ? 1 : -1)),
                this.thousandDelimiter
                  ? (i += "#".concat(this.thousandDelimiter, "##0"))
                  : (i += "0"),
                o)
              )
                for (i += this.decimalDelimiter, r = 0; r < o; r++) i += "#";
              return i + (n ? "A" : "");
            }
          }
        ]),
        e
      );
    })();
  (Pt.getSignificantIntegers = function (e) {
    return (function (e) {
      if (Number.isNaN(+e) || !Number.isFinite(+e)) return 1;
      var t = Number(e).toExponential().split("e"),
        n = 1;
      "+" === t[1][0] && (n += Number(t[1]));
      return n;
    })(e);
  }),
    (Pt.getSignificantDecimals = function (e) {
      return (function (e) {
        if (Number.isNaN(+e) || !Number.isFinite(+e)) return 0;
        var t,
          n = Number(e).toExponential().split("e"),
          r = 0;
        "-" === n[1][0]
          ? ((r = Math.abs(Number(n[1]))),
            (n = n[0].split("."))[1] && (r += n[1].length))
          : ((t = Number(n[1])),
            (n = n[0].split(".")),
            (r = n[1] ? n[1].length - t : 0));
        return r;
      })(e);
    });
  var Rt = (function () {
    function e(t, r) {
      n(this, e), (this.localeInfo = t);
      var o = this.getFormattersForMeasures(r),
        i = at(!0, [], o);
      if (i && 1 === i.length && r[0].isCustomFormatted) i = null;
      else {
        for (var a = 0, s = 0; s < i.length; s++)
          r[s].isCustomFormatted && ((i[s] = null), a++);
        a === i.length && (i = null);
      }
      (this.measureFormatters = i),
        (this.combinedMeasuresFormatter = this.getFormatterFromFormatters(o));
    }
    return (
      o(e, [
        {
          key: "createFormatter",
          value: function () {
            return new Pt(this.localeInfo);
          }
        },
        {
          key: "getLocaleInfo",
          value: function () {
            return this.localeInfo || {};
          }
        },
        {
          key: "getFormatters",
          value: function () {
            return this.measureFormatters || [];
          }
        },
        {
          key: "getMeasureFormatter",
          value: function (e) {
            if (this.measureFormatters && this.measureFormatters[e])
              return this.measureFormatters[e];
          }
        },
        {
          key: "getCombinedMeasuresFormatter",
          value: function () {
            return this.combinedMeasuresFormatter;
          }
        },
        {
          key: "formatMeasure",
          value: function (e, t) {
            var n = this.measureFormatters[t];
            return n ? n.format(e) : e;
          }
        },
        {
          key: "getFormatterFromFormatters",
          value: function (e) {
            var t,
              n,
              r,
              o,
              i,
              a = 0,
              s = 0,
              l = 0,
              u = e
                ? e.filter(function (e) {
                    return !!e.values && !Number.isNaN(+e.values.range);
                  })
                : [];
            return (
              e &&
                e.length &&
                (e.forEach(function (e) {
                  return (
                    "U" === e.type ||
                    (e.values && Number.isNaN(+e.values.range))
                      ? s++
                      : ["D", "T", "TS", "IV"].includes(e.type)
                      ? (a = 1)
                      : (l = 1),
                    e.prepare(),
                    e
                  );
                }),
                u.length &&
                  ((t = u.reduce(function (e, t) {
                    return Math.min(t.values.min, e);
                  }, u[0].values.min)),
                  (n = u.reduce(function (e, t) {
                    return Math.max(t.values.max, e);
                  }, u[0].values.max)),
                  (r = Math.abs(n - t))),
                a + l > 1
                  ? ((i =
                      e.filter(function (e) {
                        return "datetime" !== e.type;
                      })[0] || {}),
                    (o = new Pt(
                      this.localeInfo,
                      i.pattern,
                      i.thousandDelimiter,
                      i.decimalDelimiter,
                      "U"
                    )),
                    Number.isNaN(+r) ||
                      (o.pattern = o.createPatternFromRange(t, n, !0)))
                  : a + l === 1 && 0 === s
                  ? ((i = e[0] || {}),
                    (o = new Pt(
                      this.localeInfo,
                      i.pattern,
                      i.thousandDelimiter,
                      i.decimalDelimiter,
                      i.type
                    )))
                  : ((i = (i = e.filter(function (e) {
                      return "U" !== e.type;
                    })).length
                      ? i[0]
                      : e[0] || {}),
                    (o = new Pt(
                      this.localeInfo,
                      i.pattern,
                      i.thousandDelimiter,
                      i.decimalDelimiter,
                      i.type
                    )),
                    Number.isNaN(+r) ||
                      "U" !== i.type ||
                      (o.pattern = o.createPatternFromRange(t, n, !0))),
                o.prepare()),
              o || (o = new Pt(this.localeInfo, "0")),
              o
            );
          }
        },
        {
          key: "getFormattersForMeasures",
          value: function (e, t) {
            var n = [];
            return (
              e &&
                e.length &&
                (n = (t
                  ? e.filter(function (e, n) {
                      return t.indexOf(n) >= 0;
                    })
                  : e
                ).map(function (e) {
                  var t = (function (e) {
                      return (
                        !!e.qIsAutoFormat &&
                        ["M", "D", "T", "TS", "IV"].indexOf(
                          e.qNumFormat.qType
                        ) < 0
                      );
                    })(e),
                    n = new Pt(
                      this.localeInfo,
                      e.qNumFormat.qFmt,
                      e.qNumFormat.qThou,
                      e.qNumFormat.qDec,
                      t ? "U" : e.qNumFormat.qType
                    ),
                    r = Math.abs(e.qMax - e.qMin),
                    o = Number.isNaN(+r);
                  return (
                    (n.values = { min: e.qMin, max: e.qMax, range: r }),
                    !o &&
                      t &&
                      (n.pattern = n.createPatternFromRange(
                        e.qMin,
                        e.qMax,
                        !0
                      )),
                    n.prepare(),
                    n
                  );
                }, this)),
              n
            );
          }
        }
      ]),
      e
    );
  })();
  function Ht(e) {
    return (
      "number" == typeof e &&
      !Number.isNaN(parseFloat(e)) &&
      Number.isFinite(+e)
    );
  }
  var Ut = Pe.useState,
    Bt = Pe.useEffect;
  function Wt(e) {
    var t,
      n = e.endValue,
      r = e.decimals,
      o = e.duration,
      i = l(Ut(n), 2),
      a = i[0],
      s = i[1],
      u = n - a,
      c = function e(i, l) {
        return l - t >= o
          ? (s(n), void (t = null))
          : ((i = (a + (1 - (t + o - l) / o) * u).toFixed(r)),
            s(i),
            window.requestAnimationFrame(function (t) {
              return e(i, t);
            }));
      };
    return (
      Bt(
        function () {
          a !== n &&
            window.requestAnimationFrame(function (e) {
              (t = e), c(a, e);
            });
        },
        [n]
      ),
      Ht(a) ? a.toFixed(r) : a
    );
  }
  function Zt(e) {
    var t,
      n = e.style,
      r = e.formattedValue,
      o = e.showGlyph,
      i = e.cssClass,
      a = e.titleStyle,
      s = e.measureTitle;
    return Pe.createElement(
      "div",
      { className: "sn-kpi-ellips-text" },
      Pe.createElement(
        "div",
        { className: "sn-kpi-value", style: n, tid: "kpi-value" },
        Pe.createElement(
          "div",
          { className: "sn-kpi-ellips-text" },
          Pe.createElement(
            "span",
            { title: r },
            Number.isNaN(+r)
              ? r
              : Pe.createElement(Wt, {
                  endValue: +r,
                  decimals:
                    ((t = r),
                    t.indexOf(".") > -1 ? t.length - t.indexOf(".") - 1 : 0),
                  duration: 400
                })
          )
        ),
        o
          ? Pe.createElement(
              "div",
              { className: "sn-kpi-glyph-wrapper" },
              Pe.createElement(Je, { cssClass: i })
            )
          : null
      ),
      Pe.createElement(
        "div",
        { className: "sn-kpi-title sn-kpi-ellips-text", style: a },
        s
      )
    );
  }
  (Wt.propTypes = {
    endValue: Ue.number.isRequired,
    decimals: Ue.number.isRequired,
    duration: Ue.number.isRequired
  }),
    (Zt.propTypes = {
      style: Ue.object.isRequired,
      formattedValue: Ue.number.isRequired,
      showGlyph: Ue.bool.isRequired,
      cssClass: Ue.string.isRequired,
      titleStyle: Ue.string.isRequired,
      measureTitle: Ue.string.isRequired
    });
  function $t(e) {
    return Pe.createElement(
      Ge,
      Object.assign(
        {
          d:
            "M15.7048288,0 L14.0038602,0 L9.30118227,0 C9.10106832,0 9.00101134,0.100056977 9.00101134,0.300170931 L9.00101134,1.70096861 C9.00101134,1.90108256 9.10106832,2.00113954 9.30118227,2.00113954 L12.6030625,2.00113954 L7.30004273,7.30415932 C7.19998576,7.4042163 7.19998576,7.70438723 7.30004273,7.80444421 L8.20055553,8.704957 C8.3006125,8.80501398 8.60078343,8.80501398 8.70084041,8.704957 L14.0038602,3.40193722 L14.0038602,6.70381746 C14.0038602,6.90393141 14.1039172,7.00398839 14.3040311,7.00398839 L15.7048288,7.00398839 C15.9049428,7.00398839 16.0049997,6.90393141 16.0049997,6.70381746 L16.0049997,2.00113954 L16.0049997,0.300170931 C16.0049997,0.100056977 15.9049428,0 15.7048288,0 Z M11.7,14 L2.3,14 C2.1,14 2,13.9 2,13.7 L2,4.3 C2,4.1 2.1,4 2.3,4 L7,4 L7,2 L0.9,2 C0.4,2 0,2.4 0,2.9 L0,15.2 C0,15.6 0.4,16 0.9,16 L13.1000004,16 C13.6000004,16 14,15.6999998 14,15.1999998 L14,9 L12,9 C12.0006896,12.1009211 12.0006896,13.6675878 12,13.7 C11.9989656,13.7486183 11.9,14 11.7,14 Z"
        },
        e
      )
    );
  }
  function Yt(e) {
    var t = e.linkInteractionOn,
      n = e.textAlign,
      r = e.secondaryValues,
      o = e.baseLayoutInfo,
      i = e.renderState,
      a = e.onShowVerifyDialog,
      s = e.verifyDialogLayoutInfo,
      l = e.measureTitleStyle,
      u = e.measureTitle,
      c = e.sheetLinkTitle,
      p = e.mainValue,
      f = e.verifyDialogStyle,
      h = e.sheetIsMissing,
      m = e.linkToSheet,
      d = e.layout,
      g = d.sheetLink,
      b = d.openUrlInNewTab,
      _ = "sn-kpi ".concat(t ? "sn-kpi-link" : ""),
      y = "sn-kpi-measure-wrapper sn-kpi-".concat(n),
      v = "sn-kpi-ellips-text";
    r.length &&
      o.components[1].components[1].components[0].show &&
      (v += " sn-kpi-no-shrink");
    var C = "sn-kpi-value-wrapper sn-kpi-".concat(n),
      k = "sn-kpi-click-again-overlay ".concat(
        i.showVerify ? "sn-kpi-show" : ""
      );
    return Pe.createElement(
      "div",
      { className: _ },
      Pe.createElement(
        "div",
        { className: "sn-kpi-data", onClick: a },
        o.components[0].show &&
          Pe.createElement(
            "div",
            { className: y, style: l },
            Pe.createElement(
              "div",
              {
                className: "sn-kpi-measure-title sn-kpi-ellips-text",
                title: u
              },
              Pe.createElement("span", null, u),
              t
                ? Pe.createElement($t, {
                    size: "inherit",
                    class: "sn-kpi-info-icon",
                    title: c
                  })
                : null
            )
          ),
        Pe.createElement(
          "div",
          { className: C },
          Pe.createElement("div", { className: v }, Pe.createElement(Zt, p)),
          o.components[1].components[1].components[0] &&
            o.components[1].components[1].components[0].show
            ? Pe.createElement(
                "div",
                { className: "sn-kpi-secondary-wrapper sn-kpi-ellips-text" },
                r.map(function (e) {
                  return Pe.createElement(Zt, e);
                })
              )
            : null
        )
      ),
      i.showVerify &&
        Pe.createElement(
          "div",
          {
            className: k,
            style: f,
            onClick: function () {
              return m(g, b);
            }
          },
          !h && Pe.createElement("span", null, s.components[0].getText()),
          Pe.createElement(
            "span",
            { className: "sn-kpi-sheet-link-title sn-kpi-ellips-text" },
            c
          )
        )
    );
  }
  !(function (e, t) {
    void 0 === t && (t = {});
    var n = t.insertAt;
    if (e && "undefined" != typeof document) {
      var r = document.head || document.getElementsByTagName("head")[0],
        o = document.createElement("style");
      (o.type = "text/css"),
        "top" === n && r.firstChild
          ? r.insertBefore(o, r.firstChild)
          : r.appendChild(o),
        o.styleSheet
          ? (o.styleSheet.cssText = e)
          : o.appendChild(document.createTextNode(e));
    }
  })(
    ".qv-object-kpi .qv-inner-object,\n.qv-object-sn-kpi-chart .qv-inner-object {\n  padding: 0 !important;\n}\n.qv-object-kpi .qv-inner-object .qv-object-header.thin,\n.qv-object-sn-kpi-chart .qv-inner-object .qv-object-header.thin {\n  height: 0px !important;\n}\n.sn-kpi.sn-kpi-link {\n  cursor: pointer;\n  box-sizing: border-box;\n  border: 2px solid transparent;\n}\n.sn-kpi.sn-kpi-link:hover {\n  border: 2px solid #cccccc;\n  border-radius: 4px;\n}\n.sn-kpi.sn-kpi-link:active {\n  border: 2px solid #595959;\n}\n.sn-kpi {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  width: 100%;\n  height: 100%;\n  transform: translate3d(0, 0, 0);\n  user-select: none;\n}\n.sn-kpi .sn-kpi-data {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  width: 100%;\n  height: 100%;\n  font-size: 0.76923077em;\n}\n.sn-kpi .sn-kpi-data > div {\n  display: flex;\n  flex-direction: row;\n}\n.sn-kpi .sn-kpi-secondary-wrapper {\n  display: flex;\n}\n.sn-kpi .sn-kpi-info-icon {\n  vertical-align: middle !important;\n  font-size: 0.66em;\n  margin-bottom: 0.17em;\n  margin-left: 8px;\n}\n.sn-kpi div {\n  flex: 0 1 auto;\n}\n.sn-kpi .qv-inner-object > div {\n  display: flex;\n  flex-direction: row;\n}\n.sn-kpi .sn-kpi-value-wrapper {\n  align-items: center;\n}\n.sn-kpi .sn-kpi-value-wrapper .sn-kpi-no-shrink {\n  flex: 0 0 auto;\n  max-width: 80%;\n}\n.sn-kpi .sn-kpi-value {\n  display: flex;\n}\n.sn-kpi .sn-kpi-ellips-text {\n  height: auto;\n  line-height: initial;\n  white-space: nowrap;\n  -webkit-text-overflow: ellipsis;\n  text-overflow: ellipsis;\n  overflow: hidden;\n}\n.sn-kpi .sn-kpi-glyph-wrapper {\n  display: flex;\n  justify-content: center;\n  align-content: center;\n  font-size: 32%;\n  line-height: 352%;\n  flex: 1 0 auto;\n  padding: 0 0.25em;\n}\n.sn-kpi .sn-kpi-glyph-wrapper .sn-kpi-value-icon {\n  line-height: inherit !important;\n}\n.sn-kpi .sn-kpi-left {\n  justify-content: flex-start;\n}\n.sn-kpi .sn-kpi-center {\n  justify-content: center;\n}\n.sn-kpi .sn-kpi-right {\n  justify-content: flex-end;\n}\n.sn-kpi .sn-kpi-click-again-overlay {\n  position: absolute;\n  background: rgba(0, 0, 0, 0.7);\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  border-radius: 6px;\n  color: #fff;\n  display: flex;\n  visibility: hidden;\n  opacity: 0;\n  align-items: center;\n  justify-content: center;\n  flex-direction: column;\n  font-size: 13px;\n  text-align: center;\n}\n.sn-kpi .sn-kpi-click-again-overlay .sn-kpi-sheet-link-title {\n  font-size: 1.7em;\n  max-width: 100%;\n}\n.sn-kpi .sn-kpi-click-again-overlay.sn-kpi-show {\n  visibility: visible;\n  animation: fadeInOpacity 0.3s linear forwards;\n}\n@-webkit-keyframes fadeInOpacity {\n  100% {\n    opacity: 1;\n  }\n}\n@-moz-keyframes fadeInOpacity {\n  100% {\n    opacity: 1;\n  }\n}\n@-ms-keyframes fadeInOpacity {\n  100% {\n    opacity: 1;\n  }\n}\n@-o-keyframes fadeInOpacity {\n  100% {\n    opacity: 1;\n  }\n}\n@keyframes fadeInOpacity {\n  100% {\n    opacity: 1;\n  }\n}\n"
  ),
    (Yt.propTypes = {
      linkInteractionOn: Ue.bool.isRequired,
      textAlign: Ue.string.isRequired,
      secondaryValues: Ue.array.isRequired,
      baseLayoutInfo: Ue.object.isRequired,
      renderState: Ue.object.isRequired,
      onShowVerifyDialog: Ue.func.isRequired,
      showMeasureTitle: Ue.bool.isRequired,
      verifyDialogLayoutInfo: Ue.object.isRequired,
      measureTitleStyle: Ue.object.isRequired,
      measureTitle: Ue.string.isRequired,
      sheetLinkTitle: Ue.string.isRequired,
      mainValue: Ue.object.isRequired,
      verifyDialogStyle: Ue.object.isRequired,
      sheetIsMissing: Ue.bool.isRequired,
      linkToSheet: Ue.func.isRequired,
      layout: Ue.object.isRequired
    });
  var Gt = [
      { key: "m", value: "lui-icon--tick" },
      { key: "ï", value: "lui-icon--star" },
      { key: "R", value: "lui-icon--triangle-top" },
      { key: "S", value: "lui-icon--triangle-bottom" },
      { key: "T", value: "lui-icon--triangle-left" },
      { key: "U", value: "lui-icon--triangle-right" },
      { key: "P", value: "lui-icon--plus" },
      { key: "Q", value: "lui-icon--minus" },
      { key: "è", value: "lui-icon--warning-triangle" },
      { key: "¢", value: "lui-icon--hand" },
      { key: "©", value: "lui-icon--flag" },
      { key: "23F4", value: "lui-icon--lightbulb" },
      { key: "2013", value: "lui-icon--stop" },
      { key: "&", value: "lui-icon--pie-chart" },
      { key: "add", value: "lui-icon--add" },
      { key: "minus-2", value: "lui-icon--minus-2" },
      { key: "dot", value: "lui-icon--dot" }
    ],
    Xt = function (e) {
      var t,
        n = null;
      if (null != e)
        if (0 === e.length) n = "";
        else
          for (t = 0; t < Gt.length; t++)
            if (Gt[t].key === e) {
              n = Gt[t].value;
              break;
            }
      return n;
    },
    Jt = {
      aliceblue: { r: 240, g: 248, b: 255 },
      antiquewhite: { r: 250, g: 235, b: 215 },
      aqua: { r: 0, g: 255, b: 255 },
      aquamarine: { r: 127, g: 255, b: 212 },
      azure: { r: 240, g: 255, b: 255 },
      beige: { r: 245, g: 245, b: 220 },
      bisque: { r: 255, g: 228, b: 196 },
      black: { r: 0, g: 0, b: 0 },
      blanchedalmond: { r: 255, g: 235, b: 205 },
      blue: { r: 0, g: 0, b: 255 },
      blueviolet: { r: 138, g: 43, b: 226 },
      brown: { r: 165, g: 42, b: 42 },
      burlywood: { r: 222, g: 184, b: 135 },
      cadetblue: { r: 95, g: 158, b: 160 },
      chartreuse: { r: 127, g: 255, b: 0 },
      chocolate: { r: 210, g: 105, b: 30 },
      coral: { r: 255, g: 127, b: 80 },
      cornflowerblue: { r: 100, g: 149, b: 237 },
      cornsilk: { r: 255, g: 248, b: 220 },
      crimson: { r: 220, g: 20, b: 60 },
      cyan: { r: 0, g: 255, b: 255 },
      darkblue: { r: 0, g: 0, b: 139 },
      darkcyan: { r: 0, g: 139, b: 139 },
      darkgoldenrod: { r: 184, g: 134, b: 11 },
      darkgray: { r: 169, g: 169, b: 169 },
      darkgreen: { r: 0, g: 100, b: 0 },
      darkgrey: { r: 169, g: 169, b: 169 },
      darkkhaki: { r: 189, g: 183, b: 107 },
      darkmagenta: { r: 139, g: 0, b: 139 },
      darkolivegreen: { r: 85, g: 107, b: 47 },
      darkorange: { r: 255, g: 140, b: 0 },
      darkorchid: { r: 153, g: 50, b: 204 },
      darkred: { r: 139, g: 0, b: 0 },
      darksalmon: { r: 233, g: 150, b: 122 },
      darkseagreen: { r: 143, g: 188, b: 143 },
      darkslateblue: { r: 72, g: 61, b: 139 },
      darkslategray: { r: 47, g: 79, b: 79 },
      darkslategrey: { r: 47, g: 79, b: 79 },
      darkturquoise: { r: 0, g: 206, b: 209 },
      darkviolet: { r: 148, g: 0, b: 211 },
      deeppink: { r: 255, g: 20, b: 147 },
      deepskyblue: { r: 0, g: 191, b: 255 },
      dimgray: { r: 105, g: 105, b: 105 },
      dimgrey: { r: 105, g: 105, b: 105 },
      dodgerblue: { r: 30, g: 144, b: 255 },
      firebrick: { r: 178, g: 34, b: 34 },
      floralwhite: { r: 255, g: 250, b: 240 },
      forestgreen: { r: 34, g: 139, b: 34 },
      fuchsia: { r: 255, g: 0, b: 255 },
      gainsboro: { r: 220, g: 220, b: 220 },
      ghostwhite: { r: 248, g: 248, b: 255 },
      gold: { r: 255, g: 215, b: 0 },
      goldenrod: { r: 218, g: 165, b: 32 },
      gray: { r: 128, g: 128, b: 128 },
      green: { r: 0, g: 128, b: 0 },
      greenyellow: { r: 173, g: 255, b: 47 },
      grey: { r: 128, g: 128, b: 128 },
      honeydew: { r: 240, g: 255, b: 240 },
      hotpink: { r: 255, g: 105, b: 180 },
      indianred: { r: 205, g: 92, b: 92 },
      indigo: { r: 75, g: 0, b: 130 },
      ivory: { r: 255, g: 255, b: 240 },
      khaki: { r: 240, g: 230, b: 140 },
      lavender: { r: 230, g: 230, b: 250 },
      lavenderblush: { r: 255, g: 240, b: 245 },
      lawngreen: { r: 124, g: 252, b: 0 },
      lemonchiffon: { r: 255, g: 250, b: 205 },
      lightblue: { r: 173, g: 216, b: 230 },
      lightcoral: { r: 240, g: 128, b: 128 },
      lightcyan: { r: 224, g: 255, b: 255 },
      lightgoldenrodyellow: { r: 250, g: 250, b: 210 },
      lightgray: { r: 211, g: 211, b: 211 },
      lightgreen: { r: 144, g: 238, b: 144 },
      lightgrey: { r: 211, g: 211, b: 211 },
      lightpink: { r: 255, g: 182, b: 193 },
      lightsalmon: { r: 255, g: 160, b: 122 },
      lightseagreen: { r: 32, g: 178, b: 170 },
      lightskyblue: { r: 135, g: 206, b: 250 },
      lightslategray: { r: 119, g: 136, b: 153 },
      lightslategrey: { r: 119, g: 136, b: 153 },
      lightsteelblue: { r: 176, g: 196, b: 222 },
      lightyellow: { r: 255, g: 255, b: 224 },
      lime: { r: 0, g: 255, b: 0 },
      limegreen: { r: 50, g: 205, b: 50 },
      linen: { r: 250, g: 240, b: 230 },
      magenta: { r: 255, g: 0, b: 255 },
      maroon: { r: 128, g: 0, b: 0 },
      mediumaquamarine: { r: 102, g: 205, b: 170 },
      mediumblue: { r: 0, g: 0, b: 205 },
      mediumorchid: { r: 186, g: 85, b: 211 },
      mediumpurple: { r: 147, g: 112, b: 219 },
      mediumseagreen: { r: 60, g: 179, b: 113 },
      mediumslateblue: { r: 123, g: 104, b: 238 },
      mediumspringgreen: { r: 0, g: 250, b: 154 },
      mediumturquoise: { r: 72, g: 209, b: 204 },
      mediumvioletred: { r: 199, g: 21, b: 133 },
      midnightblue: { r: 25, g: 25, b: 112 },
      mintcream: { r: 245, g: 255, b: 250 },
      mistyrose: { r: 255, g: 228, b: 225 },
      moccasin: { r: 255, g: 228, b: 181 },
      navajowhite: { r: 255, g: 222, b: 173 },
      navy: { r: 0, g: 0, b: 128 },
      oldlace: { r: 253, g: 245, b: 230 },
      olive: { r: 128, g: 128, b: 0 },
      olivedrab: { r: 107, g: 142, b: 35 },
      orange: { r: 255, g: 165, b: 0 },
      orangered: { r: 255, g: 69, b: 0 },
      orchid: { r: 218, g: 112, b: 214 },
      palegoldenrod: { r: 238, g: 232, b: 170 },
      palegreen: { r: 152, g: 251, b: 152 },
      paleturquoise: { r: 175, g: 238, b: 238 },
      palevioletred: { r: 219, g: 112, b: 147 },
      papayawhip: { r: 255, g: 239, b: 213 },
      peachpuff: { r: 255, g: 218, b: 185 },
      peru: { r: 205, g: 133, b: 63 },
      pink: { r: 255, g: 192, b: 203 },
      plum: { r: 221, g: 160, b: 221 },
      powderblue: { r: 176, g: 224, b: 230 },
      purple: { r: 128, g: 0, b: 128 },
      red: { r: 255, g: 0, b: 0 },
      rosybrown: { r: 188, g: 143, b: 143 },
      royalblue: { r: 65, g: 105, b: 225 },
      saddlebrown: { r: 139, g: 69, b: 19 },
      salmon: { r: 250, g: 128, b: 114 },
      sandybrown: { r: 244, g: 164, b: 96 },
      seagreen: { r: 46, g: 139, b: 87 },
      seashell: { r: 255, g: 245, b: 238 },
      sienna: { r: 160, g: 82, b: 45 },
      silver: { r: 192, g: 192, b: 192 },
      skyblue: { r: 135, g: 206, b: 235 },
      slateblue: { r: 106, g: 90, b: 205 },
      slategray: { r: 112, g: 128, b: 144 },
      slategrey: { r: 112, g: 128, b: 144 },
      snow: { r: 255, g: 250, b: 250 },
      springgreen: { r: 0, g: 255, b: 127 },
      steelblue: { r: 70, g: 130, b: 180 },
      tan: { r: 210, g: 180, b: 140 },
      teal: { r: 0, g: 128, b: 128 },
      thistle: { r: 216, g: 191, b: 216 },
      tomato: { r: 255, g: 99, b: 71 },
      transparent: { r: 255, g: 255, b: 255, a: 0 },
      turquoise: { r: 64, g: 224, b: 208 },
      violet: { r: 238, g: 130, b: 238 },
      wheat: { r: 245, g: 222, b: 179 },
      white: { r: 255, g: 255, b: 255 },
      whitesmoke: { r: 245, g: 245, b: 245 },
      yellow: { r: 255, g: 255, b: 0 },
      yellowgreen: { r: 154, g: 205, b: 50 }
    },
    Kt = /^rgb\((\d{1,3}),(\d{1,3}),(\d{1,3})\)$/i,
    Qt = /^rgba\((\d{1,3}),(\d{1,3}),(\d{1,3}),(\d(\.\d+)?)\)$/i,
    en = /^#([A-f0-9]{2})([A-f0-9]{2})([A-f0-9]{2})$/i,
    tn = /^#([A-f0-9])([A-f0-9])([A-f0-9])$/i,
    nn = /^hsl\(\s*(\d+(\.\d+)?)\s*,\s*(\d+(\.\d+)?%?)\s*,\s*(\d+(\.\d+)?%?)\s*\)$/i,
    rn = /^hsla\(\s*(\d+(\.\d+)?)\s*,\s*(\d+(\.\d+)?%?)\s*,\s*(\d+(\.\d+)?%?)\s*,(\d(\.\d+)?)\)$/i,
    on = Math.floor,
    an = Math.round,
    sn = (function () {
      function e() {
        n(this, e);
        var t,
          r,
          o,
          i,
          a,
          s,
          l,
          u,
          c,
          p,
          f,
          h,
          m = 0,
          d = 0,
          g = 0,
          b = 1;
        if (((this._invalid = !1), arguments[0] instanceof e))
          (m = arguments[0]._r),
            (d = arguments[0]._g),
            (g = arguments[0]._b),
            (b = arguments[0]._a),
            (this._invalid = arguments[0]._invalid);
        else if (arguments.length < 3)
          if ("string" == typeof arguments[0])
            if (
              ((h = arguments[0]),
              (f = /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/i.exec(
                h
              )))
            )
              (m = parseInt(f[1], 10)),
                (d = parseInt(f[2], 10)),
                (g = parseInt(f[3], 10));
            else if (
              (f = /^rgba\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d(\.\d+)?)\s*\)$/i.exec(
                h
              ))
            )
              (m = parseInt(f[1], 10)),
                (d = parseInt(f[2], 10)),
                (g = parseInt(f[3], 10)),
                (b = parseFloat(f[4]));
            else if (
              (f = /^#([A-f0-9]{2})([A-f0-9]{2})([A-f0-9]{2})$/i.exec(h))
            )
              (m = parseInt(f[1], 16)),
                (d = parseInt(f[2], 16)),
                (g = parseInt(f[3], 16)),
                (b = 1);
            else if ((f = /^#([A-f0-9])([A-f0-9])([A-f0-9])$/i.exec(h)))
              (m = parseInt(f[1] + f[1], 16)),
                (d = parseInt(f[2] + f[2], 16)),
                (g = parseInt(f[3] + f[3], 16)),
                (b = 1);
            else if (
              (f = /^hsl\(\s*(\d+(\.\d+)?)\s*,\s*(\d+(\.\d+)?%?)\s*,\s*(\d+(\.\d+)?%?)\s*\)$/i.exec(
                h
              ))
            ) {
              switch (
                ((t = parseFloat(f[1])),
                (r = parseFloat(f[3])),
                (i = parseFloat(f[5])),
                (r = (r /= 100) < 0 ? 0 : r > 1 ? 1 : r),
                (l = (t = (t %= 360) < 0 ? 0 : t > 360 ? 360 : t) / 60),
                (u =
                  (s =
                    (i = (i /= 100) < 0 ? 0 : i > 1 ? 1 : i) <= 0.5
                      ? 2 * i * r
                      : (2 - 2 * i) * r) *
                  (1 - Math.abs((l % 2) - 1))),
                (c = []),
                (l = Math.floor(l)))
              ) {
                case 0:
                  c = [s, u, 0];
                  break;
                case 1:
                  c = [u, s, 0];
                  break;
                case 2:
                  c = [0, s, u];
                  break;
                case 3:
                  c = [0, u, s];
                  break;
                case 4:
                  c = [u, 0, s];
                  break;
                case 5:
                  c = [s, 0, u];
                  break;
                default:
                  c = [0, 0, 0];
              }
              (p = i - 0.5 * s),
                (m = c[0] + p),
                (d = c[1] + p),
                (g = c[2] + p),
                (m = an(255 * m)),
                (d = an(255 * d)),
                (g = an(255 * g)),
                (b = 1);
            } else if (
              (f = /^hsla\(\s*(\d+(\.\d+)?)\s*,\s*(\d+(\.\d+)?%?)\s*,\s*(\d+(\.\d+)?%?)\s*,(\d(\.\d+)?)\)$/i.exec(
                h
              ))
            ) {
              switch (
                ((t = parseFloat(f[1])),
                (r = parseFloat(f[3])),
                (i = parseFloat(f[5])),
                (b = parseFloat(f[7])),
                (r = (r /= 100) < 0 ? 0 : r > 1 ? 1 : r),
                (l = (t = (t %= 360) < 0 ? 0 : t > 360 ? 360 : t) / 60),
                (u =
                  (s =
                    (i = (i /= 100) < 0 ? 0 : i > 1 ? 1 : i) <= 0.5
                      ? 2 * i * r
                      : (2 - 2 * i) * r) *
                  (1 - Math.abs((l % 2) - 1))),
                (c = []),
                (l = Math.floor(l)))
              ) {
                case 0:
                  c = [s, u, 0];
                  break;
                case 1:
                  c = [u, s, 0];
                  break;
                case 2:
                  c = [0, s, u];
                  break;
                case 3:
                  c = [0, u, s];
                  break;
                case 4:
                  c = [u, 0, s];
                  break;
                case 5:
                  c = [s, 0, u];
                  break;
                default:
                  c = [0, 0, 0];
              }
              (p = i - 0.5 * s),
                (m = c[0] + p),
                (d = c[1] + p),
                (g = c[2] + p),
                (m = an(255 * m)),
                (d = an(255 * d)),
                (g = an(255 * g));
            } else if (
              (f = /^hsv\(\s*(\d+(\.\d+)?)\s*,\s*(\d+(\.\d+)?%?)\s*,\s*(\d+(\.\d+)?%?)\s*\)$/i.exec(
                h
              ))
            ) {
              switch (
                ((t = parseFloat(f[1])),
                (r = parseFloat(f[3])),
                (a = parseFloat(f[5])),
                (l = (t = (t %= 360) < 0 ? 0 : t > 360 ? 360 : t) / 60),
                (u =
                  (s =
                    (a = (a /= 100) < 0 ? 0 : a > 1 ? 1 : a) *
                    (r = (r /= 100) < 0 ? 0 : r > 1 ? 1 : r)) *
                  (1 - Math.abs((l % 2) - 1))),
                (c = []),
                (l = Math.floor(l)))
              ) {
                case 0:
                  c = [s, u, 0];
                  break;
                case 1:
                  c = [u, s, 0];
                  break;
                case 2:
                  c = [0, s, u];
                  break;
                case 3:
                  c = [0, u, s];
                  break;
                case 4:
                  c = [u, 0, s];
                  break;
                case 5:
                  c = [s, 0, u];
                  break;
                default:
                  c = [0, 0, 0];
              }
              (p = a - s),
                (m = c[0] + p),
                (d = c[1] + p),
                (g = c[2] + p),
                (m = an(255 * m)),
                (d = an(255 * d)),
                (g = an(255 * g)),
                (b = 1);
            } else
              Jt[h.toLowerCase()]
                ? ((o = h.toLowerCase()),
                  (m = Jt[o].r),
                  (d = Jt[o].g),
                  (g = Jt[o].b),
                  (b = "number" == typeof Jt[o].a ? Jt[o].a : 1))
                : (this._invalid = !0);
          else
            "number" == typeof arguments[0] &&
            arguments[0] >= 0 &&
            "argb" === arguments[1]
              ? ((b = (4278190080 & arguments[0]) >>> 24),
                (b /= 255),
                (m = (16711680 & arguments[0]) >> 16),
                (d = (65280 & arguments[0]) >> 8),
                (g = 255 & arguments[0]))
              : "number" == typeof arguments[0] && arguments[0] >= 0
              ? ((m = (16711680 & arguments[0]) >> 16),
                (d = (65280 & arguments[0]) >> 8),
                (g = 255 & arguments[0]))
              : (this._invalid = !0);
        else
          arguments.length >= 3
            ? ((m = arguments[0]),
              (d = arguments[1]),
              (g = arguments[2]),
              (b = arguments.length >= 4 ? arguments[3] : 1))
            : (this._invalid = !0);
        Number.isNaN(+m + d + g + b) && (this._invalid = !0),
          (this._r = on(m)),
          (this._g = on(d)),
          (this._b = on(g)),
          (this._a = b),
          (this._spaces = {});
      }
      return (
        o(e, [
          {
            key: "isInvalid",
            value: function () {
              return this._invalid;
            }
          },
          {
            key: "setAlpha",
            value: function (e) {
              (this._a = e), (this._spaces = {});
            }
          },
          {
            key: "getAlpha",
            value: function () {
              return this._a;
            }
          },
          {
            key: "toRGB",
            value: function () {
              return (
                this._spaces.rgb || (this._spaces.rgb = e.toRGB(this)),
                this._spaces.rgb
              );
            }
          },
          {
            key: "toRGBA",
            value: function () {
              return (
                this._spaces.rgba || (this._spaces.rgba = e.toRGBA(this)),
                this._spaces.rgba
              );
            }
          },
          {
            key: "toString",
            value: function () {
              return (
                this._spaces.rgb || (this._spaces.rgb = e.toRGB(this)),
                this._spaces.rgb
              );
            }
          },
          {
            key: "toHex",
            value: function () {
              return (
                this._spaces.hex || (this._spaces.hex = e.toHex(this)),
                this._spaces.hex
              );
            }
          },
          {
            key: "toHSL",
            value: function (t) {
              return (
                this._spaces.hsl || (this._spaces.hsl = e.toHSL(this, t)),
                this._spaces.hsl
              );
            }
          },
          {
            key: "toHSLA",
            value: function (t) {
              return (
                this._spaces.hsla || (this._spaces.hsla = e.toHSLA(this, t)),
                this._spaces.hsla
              );
            }
          },
          {
            key: "toHSV",
            value: function () {
              var e = this.toHSVComponents();
              return "hsv(".concat(e.h, ",").concat(e.s, ",").concat(e.v, ")");
            }
          },
          {
            key: "toHSVComponents",
            value: function () {
              return (
                this._spaces.hsvComp ||
                  (this._spaces.hsvComp = e.toHSVComponents(this)),
                this._spaces.hsvComp
              );
            }
          },
          {
            key: "toNumber",
            value: function () {
              return (
                this._spaces.num || (this._spaces.num = e.toNumber(this)),
                this._spaces.num
              );
            }
          },
          {
            key: "isDark",
            value: function () {
              return this.isInvalid() || this.getLuminance() < 125;
            }
          },
          {
            key: "getLuminance",
            value: function () {
              return (
                void 0 === this._lumi &&
                  (this._lumi =
                    0.299 * this._r + 0.587 * this._g + 0.114 * this._b),
                this._lumi
              );
            }
          },
          {
            key: "shiftLuminance",
            value: function (e) {
              var t = this.toHSLA(),
                n = /^hsla\(\s*(\d+(\.\d+)?)\s*,\s*(\d+(\.\d+)?%?)\s*,\s*(\d+(\.\d+)?%?)\s*,(\d(\.\d+)?)\)$/i.exec(
                  t
                ),
                r = parseFloat(n[1]),
                o = parseFloat(n[3]),
                i = parseFloat(n[5]),
                a = parseFloat(n[7]);
              return (
                (i += e),
                (i = Math.max(0, Math.min(i, 100))),
                "hsla("
                  .concat(r, ",")
                  .concat(o, ",")
                  .concat(i, ",")
                  .concat(a, ")")
              );
            }
          },
          {
            key: "isEqual",
            value: function (t) {
              return (
                t instanceof e || (t = new e(t)),
                this._r === t._r && this._g === t._g && this._b === t._b
              );
            }
          },
          {
            key: "blend",
            value: function (e, t) {
              var n = on(this._r + (e._r - this._r) * t),
                r = on(this._g + (e._g - this._g) * t),
                o = on(this._b + (e._b - this._b) * t),
                i = on(this._a + (e._a - this._a) * t);
              return "rgba(".concat([n, r, o, i].join(","), ")");
            }
          },
          {
            key: "createShiftedColor",
            value: function (t) {
              (void 0 === t || Number.isNaN(+t)) && (t = 1);
              var n = this.getLuminance(),
                r = this._g < 126 ? 1 : 1 + this._g / 512;
              return new e(
                this.shiftLuminance(
                  20 *
                    (n * r < 220
                      ? 0.8 + (1 / (n + 1)) * 4
                      : -(0.8 + (n / 255) * 1)) *
                    t
                )
              );
            }
          }
        ]),
        e
      );
    })();
  (sn.toRGB = function (e) {
    if (e instanceof sn)
      return "rgb(".concat([e._r, e._g, e._b].join(","), ")");
    "string" == typeof e && (e = sn.toNumber(e));
    var t = (65280 & e) >> 8,
      n = 255 & e;
    return "rgb("
      .concat((16711680 & e) >> 16, ",")
      .concat(t, ",")
      .concat(n, ")");
  }),
    (sn.toRGBA = function (e, t) {
      if (e instanceof sn)
        return "rgba(".concat(
          [e._r, e._g, e._b, void 0 !== t ? t : e._a].join(","),
          ")"
        );
      "string" == typeof e && (e = sn.toNumber(e));
      var n = (65280 & e) >> 8,
        r = 255 & e;
      return "rgba("
        .concat((16711680 & e) >> 16, ",")
        .concat(n, ",")
        .concat(r, ",")
        .concat(void 0 !== t ? t : e._a, ")");
    }),
    (sn.toHex = function (e) {
      var t, n, r;
      return e instanceof sn
        ? ((t = e._r.toString(16)),
          (n = e._g.toString(16)),
          (r = e._b.toString(16)),
          1 === t.length && (t = "0".concat(t)),
          1 === n.length && (n = "0".concat(n)),
          1 === r.length && (r = "0".concat(r)),
          "#".concat([t, n, r].join("")))
        : ("string" == typeof e && (e = sn.toNumber(e)),
          (t = ((16711680 & e) >> 16).toString(16)),
          (n = ((65280 & e) >> 8).toString(16)),
          (r = (255 & e).toString(16)),
          1 === t.length && (t = "0".concat(t)),
          1 === n.length && (n = "0".concat(n)),
          1 === r.length && (r = "0".concat(r)),
          "#".concat(t).concat(n).concat(r));
    }),
    (sn.toHSL = function (e, t) {
      var n,
        r,
        o,
        i = 0;
      "string" == typeof e && (e = new sn(e));
      var a = e._r / 255,
        s = e._g / 255,
        l = e._b / 255,
        u = Math.max(a, s, l),
        c = Math.min(a, s, l);
      if (((r = t ? 0.21 * a + 0.72 * s + 0.07 * l : (u + c) / 2), u === c))
        (n = 0), (i = 0);
      else {
        switch (
          ((o = u - c), (n = r > 0.5 ? o / (2 - u - c) : o / (u + c)), u)
        ) {
          case a:
            i = (s - l) / o + (s < l ? 6 : 0);
            break;
          case s:
            i = (l - a) / o + 2;
            break;
          case l:
            i = (a - s) / o + 4;
        }
        i /= 6;
      }
      return "hsl("
        .concat(360 * i, ",")
        .concat(100 * n, ",")
        .concat(100 * r, ")");
    }),
    (sn.toHSLA = function (e, t) {
      var n,
        r,
        o,
        i = 0;
      "string" == typeof e && (e = new sn(e));
      var a = e._r / 255,
        s = e._g / 255,
        l = e._b / 255,
        u = e._a,
        c = Math.max(a, s, l),
        p = Math.min(a, s, l);
      if (((r = t ? 0.21 * a + 0.72 * s + 0.07 * l : (c + p) / 2), c === p))
        (n = 0), (i = 0);
      else {
        switch (
          ((o = c - p), (n = r > 0.5 ? o / (2 - c - p) : o / (c + p)), c)
        ) {
          case a:
            i = (s - l) / o + (s < l ? 6 : 0);
            break;
          case s:
            i = (l - a) / o + 2;
            break;
          case l:
            i = (a - s) / o + 4;
        }
        i /= 6;
      }
      return "hsla("
        .concat(360 * i, ",")
        .concat(100 * n, ",")
        .concat(100 * r, ",")
        .concat(u, ")");
    }),
    (sn.toHSVComponents = function (e) {
      var t,
        n,
        r = 0;
      "string" == typeof e && (e = new sn(e));
      var o = e._r / 255,
        i = e._g / 255,
        a = e._b / 255,
        s = Math.max(o, i, a),
        l = Math.min(o, i, a),
        u = s;
      if (s === l) (t = 0), (r = 0);
      else {
        switch (((t = 0 === (n = s - l) ? 0 : n / u), s)) {
          case o:
            r = (i - a) / n + (i < a ? 6 : 0);
            break;
          case i:
            r = (a - o) / n + 2;
            break;
          case a:
            r = (o - i) / n + 4;
        }
        r /= 6;
      }
      return { h: (360 * r) % 360, s: 100 * t, v: 100 * u };
    }),
    (sn.toNumber = function () {
      if (1 === arguments.length && arguments[0] instanceof sn)
        return (
          (arguments[0]._r << 16) + (arguments[0]._g << 8) + arguments[0]._b
        );
      var e,
        t,
        n = 0,
        r = 0,
        o = 0;
      return (
        1 === arguments.length &&
          "string" == typeof arguments[0] &&
          ((t = arguments[0]),
          (e = /^rgb\((\d{1,3}),(\d{1,3}),(\d{1,3})\)$/i.exec(t))
            ? ((n = parseInt(e[1], 10)),
              (r = parseInt(e[2], 10)),
              (o = parseInt(e[3], 10)))
            : (e = /^#([A-f0-9]{2})([A-f0-9]{2})([A-f0-9]{2})$/i.exec(t))
            ? ((n = parseInt(e[1], 16)),
              (r = parseInt(e[2], 16)),
              (o = parseInt(e[3], 16)))
            : (e = /^#([A-f0-9])([A-f0-9])([A-f0-9])$/i.exec(t)) &&
              ((n = parseInt(e[1] + e[1], 16)),
              (r = parseInt(e[2] + e[2], 16)),
              (o = parseInt(e[3] + e[3], 16)))),
        (n << 16) + (r << 8) + o
      );
    }),
    (sn.blend = function (e, t, n) {
      var r = (16711680 & (e = sn.toNumber(e))) >> 16,
        o = (65280 & e) >> 8,
        i = 255 & e;
      return (
        ((r + (((16711680 & (t = sn.toNumber(t))) >> 16) - r) * n) << 16) +
        ((o + (((65280 & t) >> 8) - o) * n) << 8) +
        (i + ((255 & t) - i) * n)
      );
    }),
    (sn.getBlend = function (e, t, n) {
      var r = e._r + (t._r - e._r) * n,
        o = e._g + (t._g - e._g) * n,
        i = e._b + (t._b - e._b) * n,
        a = e._a + (t._a - e._a) * n;
      return new sn(r, o, i, a);
    }),
    (sn.isCSSColor = function (e) {
      return (
        !(arguments.length > 1 || "string" != typeof e) &&
        (Kt.test(e) ||
          Qt.test(e) ||
          en.test(e) ||
          tn.test(e) ||
          nn.test(e) ||
          rn.test(e) ||
          Jt[e.toLowerCase()])
      );
    }),
    (sn.getBestContrast = function (e, t, n) {
      var r = e.getLuminance();
      return Math.abs(r - t.getLuminance()) > Math.abs(r - n.getLuminance())
        ? t
        : n;
    }),
    (sn.getContrast = function (e, t) {
      if (e && t) {
        var n = e.getLuminance() / 100,
          r = t.getLuminance() / 100;
        return n > r ? (n + 0.05) / (r + 0.05) : (r + 0.05) / (n + 0.05);
      }
    }),
    (sn.isDark = function () {
      var e = {
        getLuminance: sn.prototype.getLuminance,
        isInvalid: sn.prototype.isInvalid
      };
      return (
        sn.prototype.init.apply(e, [].slice.call(arguments)),
        sn.prototype.isDark.call(e)
      );
    }),
    (sn.useDarkLabel = function (e, t) {
      return e._a > 0.5 ? !e.isDark() : !t;
    });
  var ln = {},
    un = {},
    cn = 0,
    pn = Math.floor;
  function fn(e, t) {
    return function () {
      var n = function () {};
      n.prototype = e.prototype;
      var r = new n(),
        o = e.apply(r, t);
      return Object(o) === o ? o : r;
    };
  }
  function hn() {
    cn++;
  }
  var mn = {
      retriveColor: function () {
        var e, t, n;
        if (arguments.length < 3)
          "string" == typeof arguments[0]
            ? (e = arguments[0])
            : "number" == typeof arguments[0] &&
              arguments[0] >= 0 &&
              (e = "".concat(arguments[0]));
        else if (arguments.length >= 3)
          for (e = arguments[0], t = 0; t > arguments.length; t++)
            e += ",".concat(arguments[t]);
        if (e && ln[e]) return ln[e];
        var r = (n = fn(sn, arguments)()).isInvalid()
          ? "INVALID"
          : ""
              .concat(n._r, ",")
              .concat(n._g, ",")
              .concat(n._b, ",")
              .concat(n._a);
        if (un[r]) n = un[r];
        else {
          if (cn >= 5e3) return n;
          (un[r] = n), hn();
        }
        return e && (ln[e] = n), n;
      },
      retriveBlend: function (e, t, n) {
        var r,
          o = pn(e._r + (t._r - e._r) * n),
          i = pn(e._g + (t._g - e._g) * n),
          a = pn(e._b + (t._b - e._b) * n),
          s = pn(e._a + (t._a - e._a) * n),
          l = "".concat(o, ",").concat(i, ",").concat(a, ",").concat(s);
        if (un[l]) r = un[l];
        else {
          if (((r = fn(sn, [o, i, a, s])()), cn >= 5e3)) return r;
          (un[l] = r), hn();
        }
        return r;
      }
    },
    dn = mn.retriveColor(13816530);
  function gn(e) {
    return (
      (e = e || this._startLevel), Math.min(e, this._colorParts.length - 1)
    );
  }
  var bn = (function () {
    function e() {
      n(this, e),
        (this._colorParts = []),
        (this._startLevel = 0),
        (this._reverse = !1),
        (this._max = 1),
        (this._min = 0),
        (this._span = 1),
        (this._invalidSpan = !1);
    }
    return (
      o(e, [
        {
          key: "addColorPart",
          value: function (e, t, n) {
            (n = n || 0),
              (this._startLevel = Math.max(n, this._startLevel)),
              this._colorParts[n] || (this._colorParts[n] = []),
              this._colorParts[n].push([
                mn.retriveColor(e),
                mn.retriveColor(t)
              ]);
          }
        },
        {
          key: "setLimits",
          value: function (e) {
            if (!e)
              return (this._limits = null), void (this._expandedLimits = null);
            (this._limits = e), (this._expandedLimits = [0].concat(e, [1]));
          }
        },
        {
          key: "setMaxMinValues",
          value: function (e, t) {
            if (!(e >= t) || Number.isNaN(+e) || Number.isNaN(+t))
              throw new Error(
                "Incorrect arguments to color-scale->setMaxMinValues"
              );
            (this._max = e), (this._min = t), (this._span = e - t);
          }
        },
        {
          key: "getColor",
          value: function (e, t) {
            var n = 0 === this._span ? 0.5 : (e - this._min) / this._span;
            if (Number.isNaN(+e) || Number.isNaN(+n)) return dn;
            t = gn.apply(this, [t]);
            var r = (function (e, t, n) {
                var r;
                if (((e = Math.min(Math.max(e, 1e-12), 0.999999999999)), n)) {
                  for (var o = 0; o < n.length && n[o] <= e; ) o++;
                  r = n.length - (o + (e - n[o - 1]) / (n[o] - n[o - 1]));
                } else r = t - e * t;
                return r;
              })(n, this._colorParts[t].length, this._expandedLimits),
              o = Math.floor(r);
            o = o === r ? o - 1 : o;
            var i = this._colorParts[t][o],
              a = i[0],
              s = i[1];
            if (e === this._min) return s;
            if (e === this._max) return a;
            var l = r - o;
            return mn.retriveBlend(a, s, l);
          }
        }
      ]),
      e
    );
  })();
  function _n(e, t) {
    return sn.toHex(sn.blend(e, t, 0.5));
  }
  var yn = {
      setupColors: function (e, t, n) {
        if (e.length !== t.length + 1 || t.length !== n.length) return !1;
        var r,
          o = [e[0]],
          i = [],
          a = t.length;
        for (r = 0; r < a; r++)
          t[r]
            ? (o.push(_n(e[r], e[r + 1])),
              o.push(_n(e[r], e[r + 1])),
              i.push(n[r]),
              r !== a - 1 &&
                (o.push(e[r + 1]),
                o.push(e[r + 1]),
                i.push((n[r] + n[r + 1]) / 2)))
            : (o.push(e[r]),
              o.push(e[r + 1]),
              i.push(n[r]),
              r !== a - 1 &&
                t[r + 1] &&
                (o.push(e[r + 1]),
                o.push(e[r + 1]),
                i.push((n[r] + n[r + 1]) / 2)));
        return o.push(e[r]), { colors: o.reverse(), limits: i };
      },
      createColorScale: function (e, t) {
        var n,
          r,
          o,
          i = new bn();
        if (t) {
          for (r = 0; r < e.length; r++)
            if ((o = e[r]))
              for (n = 0; n < o.length; n += 2)
                i.addColorPart(o[n], o[n + 1], r);
        } else for (n = 0; n < e.length; n += 2) i.addColorPart(e[n], e[n + 1]);
        return i;
      }
    },
    vn = {
      blendValue: function (e, t, n, r, o) {
        var i,
          a,
          s,
          l,
          u = 0,
          c = (function (e, t, n, r) {
            var o = at(!0, {}, e);
            o &&
              o.limits &&
              ((o.limits = o.limits.concat().sort(function (e, t) {
                return e.value - t.value;
              })),
              o.limits.forEach(function (e) {
                e.value = n === t ? 0.5 : (e.value - t) / (n - t);
              }));
            var i = o.paletteColors || o.colors;
            o &&
              i &&
              (o.localColors = i.concat().map(function (e, t) {
                return { color: kn(o, t, r) };
              }));
            return o;
          })(e, n, r, o);
        if (t <= 0) return c.localColors[0].color;
        if (t >= 1) return c.localColors[c.localColors.length - 1].color;
        for (s = 0; s < c.localColors.length; s++) {
          if (((a = c.limits[s] ? c.limits[s].value : 1), t >= u && t < a)) {
            (i = Cn(c, s, t)),
              (l = new sn(c.localColors[s].color).blend(new sn(i.color), i.t));
            break;
          }
          u = a;
        }
        return l;
      },
      getIcon: function (e, t) {
        return e.paletteColors ? e.paletteColors[t].icon : e.colors[t].icon;
      },
      getSegmentIndex: function (e, t, n) {
        if (Number.isNaN(+t)) return 0;
        var r,
          o,
          i = Ht(n) ? Math.min(n, t) : 0,
          a = e.segments.limits.concat().sort(function (e, t) {
            return e.value - t.value;
          }),
          s = e.segments.paletteColors || e.segments.colors;
        for (o = 0; o < s.length; o++) {
          if (o === s.length - 1) return o;
          if (((r = a[o].value), t < r && t >= i)) return o;
          i = r;
        }
        return 0;
      },
      shouldBlend: function (e, t) {
        if (0 === e.limits.length) return;
        var n;
        n =
          0 === t
            ? e.limits[t].gradient
            : t === e.limits.length
            ? e.limits[t - 1].gradient
            : e.limits[t].gradient || e.limits[t - 1].gradient;
        return n;
      },
      resolveColorSegment: kn,
      resolveColor: function (e, n) {
        return n.getColorPickerColor("object" === t(e) ? e : { index: e });
      },
      getAutoMinMax: function (e, t) {
        var n,
          r = e.map(function (e) {
            return e.hasOwnProperty("value") ? e.value : e;
          }),
          o = 0,
          i = (r = r.sort(function (e, t) {
            return e - t;
          })).some(function (e) {
            return Number.isNaN(+e);
          }),
          a = Number.isNaN(+t) ? (Number.isNaN(+r[0]) ? 0.5 : r[0]) : t;
        if (0 === r.length || i) o = 0 === a ? 0 : 0.5 * a;
        else {
          if (1 !== r.length)
            return (
              (n = (n = r[r.length - 1] - r[0]) > 0 ? n : 1),
              {
                min: Math.min(a, r[0]) - n / 2,
                max: Math.max(a, r[r.length - 1]) + n / 2
              }
            );
          if (0 !== (n = r[0] - a) && !Number.isNaN(+n))
            return n > 0
              ? { min: a - 0.5 * Math.abs(a), max: r[0] + 2 * n }
              : { min: r[0] + 2 * n, max: a + 0.5 * Math.abs(a) };
          o = 0 === a ? 0.5 : Math.abs(0.5 * a);
        }
        return { min: a - o, max: a + o };
      },
      _getBlendInfo: Cn
    };
  function Cn(e, t, n) {
    var r,
      o,
      i = e.limits.concat().sort(function (e, t) {
        return e.value - t.value;
      }),
      a = t > 0 ? i[t - 1] : { value: 0, gradient: !1 },
      s = e.localColors[t - (t > 0 ? 1 : 0)],
      l = i[t] ? i[t] : { value: 1, gradient: !1 },
      u = e.localColors[t + (t < e.localColors.length - 1 ? 1 : 0)],
      c = n === l.value ? 1 : (n - a.value) / (l.value - a.value);
    return (
      a && a.gradient && l && l.gradient
        ? n - a.value < l.value - n
          ? ((r = s.color), (o = 0.5 - c))
          : ((r = u.color), (o = c - 0.5))
        : a && a.gradient
        ? ((r = s.color), (o = 1 - (0.5 + c / 2)))
        : l && l.gradient
        ? ((r = u.color), (o = c / 2))
        : ((r = e.localColors[t].color), (o = n)),
      { color: r, t: o }
    );
  }
  function kn(e, t, n) {
    return vn.resolveColor(
      (function (e, t) {
        return e.paletteColors ? e.paletteColors[t] : e.colors[t].color;
      })(e, t),
      n
    );
  }
  function xn(e, t) {
    return void 0 === t.useBaseColors
      ? !e.color || "measure" === e.color.useBaseColors
      : t.useBaseColors;
  }
  var Sn = function (e, t, n, r) {
    var o,
      i,
      a = Math.abs(e),
      s = n.qHyperCube.qMeasureInfo[t].conditionalColoring;
    s ||
      (s = {
        useConditionalColoring: !1,
        singleColor: 3,
        segments: {
          limits: [],
          colors: [{ color: 2 }],
          paletteColors: [{ index: 6 }]
        }
      });
    var l,
      u = "",
      c = n.qHyperCube.qMeasureInfo[t].coloring
        ? n.qHyperCube.qMeasureInfo[t].coloring.gradient
        : null,
      p = (function (e, t) {
        var n = e.qHyperCube.qMeasureInfo[t];
        return (
          !!n &&
          (n.coloring && n.coloring.baseColor && n.coloring.baseColor.color
            ? n.coloring.baseColor
            : !(!n.baseColor || !n.baseColor.color) && n.baseColor)
        );
      })(n, t);
    if (
      s.useConditionalColoring &&
      xn(n, s) &&
      c &&
      "absolute" === c.limitType
    ) {
      o = vn.getAutoMinMax(c.limits, e);
      var f = c.limits.map(function (e) {
          return (e - o.min) / (o.max - o.min);
        }),
        h = c.colors.map(r.getColorPickerColor),
        m = yn.setupColors(h, c.breakTypes, f),
        d = yn.createColorScale(m.colors, !1);
      d.setLimits(m.limits),
        d.setMaxMinValues(o.max, o.min),
        (l = d.getColor(e).toHex());
    } else if (xn(n, s) && p) l = r.getColorPickerColor(p);
    else if (s.useConditionalColoring) {
      var g =
          (o = n.qHyperCube.qMeasureInfo[t].measureAxis) &&
          !Number.isNaN(+o.min)
            ? o.min
            : e - a,
        b = o && !Number.isNaN(+o.max) ? o.max : e + a;
      i = (e - g) / (b - g);
      var _ = vn.getSegmentIndex(s, e, g);
      (u = vn.getIcon(s.segments, _) || ""),
        (l = vn.shouldBlend(s.segments, _)
          ? vn.blendValue(s.segments, i, g, b, r)
          : vn.resolveColorSegment(s.segments, _, r));
    } else l = vn.resolveColor(s.paletteSingleColor || s.singleColor, r);
    return { color: l, icon: u, useCondColor: s.useConditionalColoring };
  };
  return function (t) {
    var n = t.sense,
      r = n && n.navigation;
    return {
      qae: { properties: u, data: p(t) },
      component: function () {
        var o = l(e.useState({}), 2),
          i = o[0],
          a = o[1],
          u = l(e.useState(), 2),
          c = u[0],
          p = u[1],
          f = e.useConstraints(),
          h = e.useTheme(),
          m = e.useOptions(),
          d = e.useElement(),
          g = e.useLayout(),
          b = e.useRect(),
          _ = e.useTranslator(),
          y = e.useAppLayout(),
          C = e.useApp(),
          k = function (e, t) {
            if (i[e] !== t) {
              var n = Object.assign({}, i);
              (n[e] = t), a(n);
            }
          };
        e.useEffect(function () {
          return function () {
            var e;
            (e = d), Pe.unmountComponentAtNode(e);
          };
        }, []),
          e.useEffect(
            function () {
              var e = (function (e, t, n, r, o) {
                var i,
                  a = { backendApi: {}, options: {} },
                  s = {};
                a.props = s;
                var l = function () {
                    n("showVerify", !1);
                  },
                  u = e.getBoundingClientRect();
                return (
                  (a.props.mainValueStyle = {}),
                  (a.props.measureTitleStyle = {}),
                  (a.props.viz = {}),
                  (a.props.secondaryValues = []),
                  (a.props.secondaryLayouts = []),
                  (a.props.showSecondaryValues = !1),
                  (a.props.mainValue = {
                    formattedValue: "",
                    value: null,
                    style: null
                  }),
                  n("showVerify", !1),
                  (s.onShowVerifyDialog = function () {
                    s.linkInteractionOn &&
                      (n("showVerify", !0),
                      s.updateVerifyDialogFontSize(),
                      (i = setTimeout(l, 3e3)));
                  }),
                  (s.linkToSheet = function (e, t) {
                    if (
                      s.linkInteractionOn &&
                      !s.sheetIsMissing &&
                      s.renderState.showVerify
                    ) {
                      if (t && o && o.getUrlForSheet) {
                        var n = o.getUrlForSheet(e);
                        window.open(n);
                      } else o.goToSheet(e);
                      clearTimeout(i), l();
                    }
                  }),
                  (s.updateVerifyDialogFontSize = function () {
                    if (s.renderState.showVerify) {
                      var e = s.verifyDialogLayoutInfo.components[0];
                      v.checkLayout(s.verifyDialogLayoutInfo),
                        v.sizeComponent(
                          e,
                          s.verifyDialogLayoutInfo,
                          s.verifyDialogLayoutInfo.boundingBox.width,
                          s.verifyDialogLayoutInfo.boundingBox.height,
                          v.TextScaleMode.SCALE,
                          v.ElementScaleMode.NONE,
                          r
                        ),
                        (s.verifyDialogStyle = {
                          fontSize: e.fontSizes.fontSize
                        });
                    }
                  }),
                  (a.props.baseLayoutInfo = {
                    direction: v.Direction.COLUMN,
                    ratio: 1e9,
                    boundingBox: { width: u.width, height: u.height },
                    components: [
                      {
                        size: { min: 0.5, treshold: { width: 50, height: 12 } },
                        getText: function () {
                          return s.measureTitle;
                        },
                        fontSizes: {
                          maxNrChars: 15,
                          min: { fontSize: 10, height: 12 },
                          max: { fontSize: 40, height: 80 }
                        },
                        isVisible: function () {
                          return s.showMeasureTitle;
                        }
                      },
                      {
                        size: { min: 0.8, stretch: !0 },
                        ratio: 1,
                        components: [
                          {
                            size: { min: 0.5, stretch: !0 },
                            getText: function () {
                              return (
                                a.props.mainValue.formattedValue +
                                (a.props.mainValue.glyph
                                  ? a.props.mainValue.glyph
                                  : "")
                              );
                            },
                            fontSizes: {
                              maxNrChars: 4,
                              min: { fontSize: 10, height: 10 },
                              max: { fontSize: 500, height: 500 }
                            }
                          },
                          {
                            size: 0.5,
                            components: a.props.secondaryLayouts,
                            isVisible: function () {
                              return a.props.secondaryLayouts.length > 0;
                            }
                          }
                        ]
                      }
                    ]
                  }),
                  (a.props.secondaryLayoutInfo = {
                    size: { min: 1, treshold: { width: 50, height: 30 } },
                    direction: v.Direction.COLUMN,
                    components: [
                      {
                        size: { min: 0.6, stretch: !0 },
                        fontSizes: {
                          maxNrChars: 8,
                          min: { fontSize: 10, height: 20 },
                          max: { fontSize: 500, height: 500 }
                        }
                      },
                      {
                        size: { min: 0.4 },
                        fontSizes: {
                          maxNrChars: 15,
                          min: { fontSize: 10, height: 20 },
                          max: { fontSize: 30, height: 500 }
                        },
                        isVisible: function () {
                          return !1 !== s.showSecondMeasureTitle;
                        }
                      }
                    ]
                  }),
                  (a.props.verifyDialogLayoutInfo = {
                    size: { min: 1, width: 20, height: 20 },
                    boundingBox: { width: 0, height: 0 },
                    components: [
                      {
                        size: {
                          min: 0.2,
                          treshold: { width: 100, height: 100 }
                        },
                        getText: function () {
                          return t.get("properties.kpi.verifyToGoSheet");
                        },
                        fontSizes: {
                          maxNrChars: 16,
                          min: { fontSize: 10, height: 20 },
                          max: { fontSize: 13, height: 50 }
                        }
                      }
                    ]
                  }),
                  a
                );
              })(d, _, k, h, r);
              (e.localeInfo = y.qLocaleInfo), (e.inClient = !!t.sense), p(e);
            },
            [d, y, h.name()]
          ),
          e.useEffect(
            function () {
              n &&
                d &&
                c &&
                g &&
                (function (e, t, n) {
                  t.cellEl
                    ? t.cellEl.removeEventListener("keydown", t.handleKeyDown)
                    : (t.cellEl = e.closest(".qv-gridcell")),
                    (t.handleKeyDown = function (e) {
                      var r = e.key;
                      ("Spacebar" !== r && " " !== r && "Enter" !== r) ||
                        (t.props.renderState.showVerify
                          ? t.props.linkToSheet(n.sheetLink, n.openUrlInNewTab)
                          : t.props.onShowVerifyDialog());
                    }),
                    t.cellEl &&
                      t.cellEl.addEventListener("keydown", t.handleKeyDown);
                })(d, c, g);
            },
            [d, c, g]
          ),
          e.useEffect(
            function () {
              var e, t;
              c &&
                g &&
                ((c.props.renderState = i),
                (c.updateRenderState = k),
                (c.navigation = !f.active),
                (c.options = m),
                (c.maxFontState = (function (e, t, n, r) {
                  return e.backendApi.isSnapshot && r && !n.freeResize
                    ? r.snapshotData.isZoomed
                    : n.isZoomed && e.inClient;
                })(c, 0, m, g)),
                (c.formatters = (function (e, t, n) {
                  for (
                    var r,
                      o,
                      i = new Rt(n, t.qHyperCube.qMeasureInfo),
                      a = [],
                      s = 0;
                    s < e;
                    s++
                  )
                    (r = t.qHyperCube.qMeasureInfo[s]),
                      (o = i.getMeasureFormatter(s)) &&
                        "U" === o.type &&
                        ((o.pattern = o.createPatternFromRange(
                          r.qMax,
                          r.qMax,
                          !0
                        )),
                        Math.floor(Math.log(Math.abs(r.qMax)) / Math.log(10)) %
                          3 ==
                          2 &&
                          ((o.pattern = o.pattern.substr(
                            0,
                            o.pattern.length - 2
                          )),
                          (o.pattern += "A")),
                        o.prepare()),
                      a.push(o);
                  return a;
                })(g.qHyperCube.qMeasureInfo.length, g, c.localeInfo)),
                (function (e, t, n, r) {
                  if (
                    !(function (e) {
                      return !(
                        !e ||
                        !e.qHyperCube ||
                        !e.qHyperCube.qMeasureInfo.length ||
                        e.qHyperCube.qMeasureInfo.some(function (e) {
                          return !!e.qError;
                        })
                      );
                    })(e)
                  )
                    return !1;
                  t.layout = e;
                  var o,
                    i,
                    a,
                    l,
                    u,
                    c,
                    p = {
                      font: {
                        baseFontSizeSmall: 7,
                        baseFontSizeMedium: 4,
                        baseFontSizeLarge: 3
                      },
                      layout: {
                        responsive: { S: 7, M: 4, L: 3 },
                        relative: { S: 10.5, M: 6, L: 3 },
                        fixed: { S: 10.5, M: 6, L: 3 }
                      }
                    },
                    f = e.qHyperCube.qDataPages[0].qMatrix[0],
                    h = e.qHyperCube.qMeasureInfo.length,
                    m = t.inClient,
                    d = t.formatters,
                    g = h - 1,
                    b = t.props.secondaryValues,
                    _ = t.props.secondaryLayouts,
                    y =
                      t.props.sheet &&
                      t.props.sheet.qMetaDef &&
                      t.props.sheet.qMetaDef.title
                        ? t.props.sheet.qMetaDef.title
                        : null,
                    v = function () {
                      return l;
                    },
                    C = function () {
                      return c;
                    };
                  (t.props.showMeasureTitle = e.showMeasureTitle),
                    (t.props.showSecondMeasureTitle = e.showSecondMeasureTitle),
                    (t.props.textAlign = e.textAlign || "center");
                  var k,
                    x,
                    S,
                    w,
                    M = e.layoutBehavior || "responsive",
                    L = t.props.baseLayoutInfo.components[0],
                    N = t.props.baseLayoutInfo.components[1];
                  "fixed" === M
                    ? ((L.fontSizes.maxNrChars =
                        3.75 * p.layout[M][e.fontSize]),
                      (N.components[0].fontSizes.maxNrChars =
                        p.layout[M][e.fontSize]))
                    : "relative" === M
                    ? ((L.fontSizes.maxNrChars =
                        3.75 * p.font.baseFontSizeMedium),
                      (N.components[0].fontSizes.maxNrChars =
                        p.layout[M][e.fontSize]))
                    : ((L.fontSizes.maxNrChars =
                        3.75 * p.font.baseFontSizeMedium),
                      (N.components[0].fontSizes.maxNrChars = 0)),
                    (N.components[0].fontSizes.scale =
                      "S" === e.fontSize
                        ? 7 / 3
                        : "L" === e.fontSize
                        ? 1
                        : Math.sqrt(7 / 3)),
                    (t.props.useLink = e.useLink),
                    !m ||
                      e.snapshotData ||
                      !t.props.useLink ||
                      !e.sheetLink ||
                      (y && e.sheetLink === y) ||
                      (function (e, t, n, r) {
                        n &&
                          n
                            .getObject(t)
                            .then(function (t) {
                              t.getLayout().then(function (t) {
                                var n = t && t.labelExpression,
                                  r = t && t.qMeta && t.qMeta.title;
                                (e.props.sheetLinkTitle = n || r || ""),
                                  (e.props.sheetIsMissing = !1);
                              });
                            })
                            .catch(function () {
                              (e.props.sheetLinkTitle = r.get(
                                "Bookmarks.SheetMissingDialogTitle"
                              )),
                                (e.props.sheetIsMissing = !0);
                            });
                      })(t, e.sheetLink, n, r),
                    (t.props.mainValue.value =
                      e.qHyperCube.qDataPages[0].qMatrix[0][0].qNum),
                    (t.props.mainValue.formattedValue =
                      d[0] &&
                      "U" === d[0].type &&
                      !Number.isNaN(
                        +e.qHyperCube.qDataPages[0].qMatrix[0][0].qNum
                      )
                        ? d[0].formatValue(t.props.mainValue.value)
                        : e.qHyperCube.qDataPages[0].qMatrix[0][0].qText),
                    (t.props.mainValue.formatter = d[0]);
                  for (var q = 0; q < g; q++) {
                    (a = f[q + 1].qText),
                      (u = f[q + 1].qNum),
                      (l =
                        !d[q + 1] || "U" !== d[q + 1].type || Number.isNaN(+u)
                          ? a
                          : d[q + 1].formatValue(u)),
                      (c = e.qHyperCube.qMeasureInfo[q + 1].qFallbackTitle),
                      b[q] ||
                        ((k = v),
                        (x = C),
                        (S = t.props.secondaryLayoutInfo),
                        (w = void 0),
                        ((w = s({}, S)).components[0].getText = k),
                        (w.components[1].getText = x),
                        (o = {
                          style: (i = { layoutInfo: w, text: k() }).layoutInfo
                            .components[0].fontSizes.value,
                          titleStyle: i.layoutInfo.components[1].fontSizes.value
                        }),
                        b.push(o),
                        _.push(i.layoutInfo)),
                      ((o = b[q]).value = u),
                      (o.formattedValue = l),
                      (o.formatter = d[q + 1]),
                      (o.measureTitle = c);
                    var D = _[q].components[0],
                      I = _[q].components[1];
                    "fixed" === M
                      ? ((D.fontSizes.maxNrChars = 2 * p.layout[M][e.fontSize]),
                        (I.fontSizes.maxNrChars =
                          3.75 * p.layout[M][e.fontSize]))
                      : ((D.fontSizes.maxNrChars = 2 * p.layout[M][e.fontSize]),
                        (I.fontSizes.maxNrChars =
                          3.75 * p.font.baseFontSizeMedium));
                  }
                  return (
                    b.length > g &&
                      (_.splice(g, b.length - g), b.splice(g, b.length - g)),
                    (t.props.measureTitle =
                      e.qHyperCube.qMeasureInfo[0].qFallbackTitle),
                    (t.props.showSecondaryValues = b.length > 0),
                    !0
                  );
                })(g, c, C, _) &&
                  (!(function (e, t, n) {
                    if (e.layout) {
                      var r,
                        o,
                        i = t.getBoundingClientRect(),
                        a = e.layout,
                        s = e.inClient;
                      (e.props.linkInteractionOn =
                        s &&
                        e.navigation &&
                        e.layout.useLink &&
                        !!e.layout.sheetLink),
                        (e.props.verifyDialogLayoutInfo.boundingBox.width =
                          i.width),
                        (e.props.verifyDialogLayoutInfo.boundingBox.height =
                          i.height),
                        (e.props.baseLayoutInfo.boundingBox = (function (e, t) {
                          if (
                            e.layout &&
                            e.layout.snapshotData &&
                            !e.options.freeResize &&
                            e.layout.snapshotData.elementRatio
                          )
                            return {
                              width: e.layout.snapshotData.object.size.w,
                              height:
                                e.layout.snapshotData.object.size.w *
                                e.layout.snapshotData.elementRatio
                            };
                          var n = t.getBoundingClientRect();
                          return { width: n.width, height: n.height };
                        })(e, t)),
                        v.checkLayout(e.props.baseLayoutInfo),
                        e.props.baseLayoutInfo.components.forEach(function (t) {
                          var r;
                          (r =
                            "fixed" === a.layoutBehavior
                              ? v.TextScaleMode.NONE
                              : 0 === e.props.secondaryValues.length &&
                                e.maxFontState
                              ? v.TextScaleMode.MAX
                              : v.TextScaleMode.SCALE),
                            v.sizeComponent(
                              t,
                              e.props.baseLayoutInfo,
                              e.props.baseLayoutInfo.boundingBox.width,
                              e.props.baseLayoutInfo.boundingBox.height,
                              r,
                              0 === e.props.secondaryValues.length &&
                                e.maxFontState
                                ? v.ElementScaleMode.ADJUSTTOTEXT
                                : v.ElementScaleMode.NONE,
                              n
                            );
                        }),
                        (o = Sn(e.props.mainValue.value, 0, a, n)),
                        (e.props.mainValue.cssClass = Xt(o.icon)),
                        (e.props.mainValue.showGlyph =
                          o.useCondColor && o.icon && "" !== o.icon);
                      var l = (r =
                        e.props.baseLayoutInfo.components[1].components[0])
                        .fontSizes.scaledFontSize;
                      (e.props.mainValue.style = {
                        color: o.color,
                        fontSize: r.fontSizes.fontSize,
                        fontFamily: n
                          ? n.getStyle(
                              "object.kpi",
                              "label.value",
                              "fontFamily"
                            )
                          : ""
                      }),
                        (r = e.props.baseLayoutInfo.components[0]),
                        (e.props.measureTitleStyle = {
                          color: n
                            ? n.getStyle("object.kpi", "label.name", "color")
                            : "#595959",
                          fontSize: r.fontSizes.fontSize,
                          fontFamily: n
                            ? n.getStyle(
                                "object.kpi",
                                "label.name",
                                "fontFamily"
                              )
                            : ""
                        });
                      for (var u = 1; u < a.qHyperCube.qMeasureInfo.length; u++)
                        (r =
                          e.props.baseLayoutInfo.components[1].components[1]
                            .components[u - 1].components[0]),
                          (o = Sn(
                            e.props.secondaryValues[u - 1].value,
                            u,
                            a,
                            n
                          )),
                          (e.props.secondaryValues[u - 1].style = {
                            color: o.color,
                            fontSize: "".concat(
                              10 * Math.min(l / 2, r.fontSizes.scaledFontSize),
                              "%"
                            )
                          }),
                          (r =
                            e.props.baseLayoutInfo.components[1].components[1]
                              .components[u - 1].components[1]),
                          (e.props.secondaryValues[u - 1].titleStyle = {
                            fontSize: "".concat(
                              10 * Math.min(l / 2, r.fontSizes.scaledFontSize),
                              "%"
                            )
                          }),
                          r.show ||
                            (e.props.secondaryValues[
                              u - 1
                            ].titleStyle.width = 0),
                          (e.props.secondaryValues[u - 1].cssClass = Xt(
                            o.icon
                          )),
                          (e.props.secondaryValues[u - 1].showGlyph =
                            o.useCondColor && o.icon && "" !== o.icon);
                      e.props.updateVerifyDialogFontSize();
                    }
                  })(c, d, h),
                  (e = d),
                  (t = s({ layout: g }, c.props)),
                  Pe.render(Pe.createElement(Yt, t), e)));
            },
            [i, g, c, b, f, h.name(), m]
          ),
          e.useImperativeHandle(function () {
            return {
              getPreferredSize: function () {
                var e = d.getBoundingClientRect();
                return Promise.resolve({
                  w: e.width(),
                  h: v.FixedMobileHeight
                });
              }
            };
          }),
          e.onTakeSnapshot(function (e) {
            var t = d.getBoundingClientRect();
            (e.snapshotData.isZoomed = c.maxFontState),
              (e.snapshotData.elementRatio = t.height / t.width);
          });
      },
      ext: c(t)
    };
  };
});
